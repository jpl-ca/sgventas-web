var map;

var active = [];
var inactive = [];
var disconnected = [];
var types = {
    agents: [],
    vehicles: []
};

var tasks = {
  all: [],
  scheduled: [],
  done: [],
  rescheduling_request: []
};

var styles = defaultMapStyle();

function initGeolocation(mapName) {

    if (mapName == null) {
        mapName = 'map';
    }

    if (map == null) {
        map = initMap(mapName);
        setMapStyles(map, styles);
        loadMarkers();
        loadTaskMarkers();
        loadPolylines();
    }
}

function loadMarkers() {

    var HTMLmarkers = $('.map-markers').find('div[data-role="marker"]');
    HTMLmarkers.each(function () {
        var lat = parseFloat($(this).data("lat"));
        var lng = parseFloat($(this).data("lng"));
        var title = $(this).data("title");
        var icon = iconBase() + $(this).data("icon");
        var status = $(this).data("status");
        var infoWindow = $(this).data("info-window");

        var marker = createMarker({lat: lat, lng: lng}, title, icon);
        setInfoBoxOnMarker(map, marker, infoWindow);

        switch (status) {
            case 'active':
                active.push(marker);
                break;
            case 'inactive':
                inactive.push(marker);
                break;
            case 'disconnected':
                disconnected.push(marker);
                break;
        }

        if (typeof type != "undefined") {
            switch (type) {
                case 'agent':
                    types.agents.push(marker);
                    break;
                case 'vehicle':
                    types.vehicles.push(marker);
                    break;
            }
        }
    });

    setMarkerOnMap(map, active);
    setMarkerOnMap(map, inactive);
    setMarkerOnMap(map, disconnected);
}

function loadTaskMarkers() {

    var HTMLmarkers = $('.map-markers').find('div[data-role="task-marker"]');
    HTMLmarkers.each(function () {
        var lat = parseFloat($(this).data("lat"));
        var lng = parseFloat($(this).data("lng"));
        var title = $(this).data("title");
        var icon = iconBase() + $(this).data("icon");
        var status = $(this).data("status");
        var infoWindow = $(this).data("info-window");
        var type = $(this).data('asset-type');

        var marker = createMarker({lat: lat, lng: lng}, title, icon);
        setInfoBoxOnMarker(map, marker, infoWindow);

        switch (status) {
            case 'scheduled':
                tasks.all.push(marker);
                tasks.scheduled.push(marker);
                break;
            case 'done':
                tasks.all.push(marker);
                tasks.done.push(marker);
                break;
            case 'rescheduling_request':
                tasks.all.push(marker);
                tasks.rescheduling_request.push(marker);
                break;
        }
    });

    setMarkerOnMap(map, tasks.all);
}

function loadPolylines() {

    var HTMPolylines = $('.map-polylines').find('div[class="polyline"]');

    HTMPolylines.each(function () {

        var HTMLpoints = $(this).find('div[data-role="polyline-point"]');

        var path = [];

        HTMLpoints.each(function () {

            var lat = parseFloat($(this).data("lat"));
            var lng = parseFloat($(this).data("lng"));

            path.push({
                lat: lat,
                lng: lng
            })

        });

        var polyline = new google.maps.Polyline({
            path: path,
            geodesic: true,
            strokeColor: '#FF1103',
            strokeOpacity: 0.90,
            strokeWeight: 4
        });

        polyline.setMap(map);

    });

}