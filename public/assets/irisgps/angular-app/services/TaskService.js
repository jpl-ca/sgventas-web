irisGpsApp.factory('TaskService', function($http) {
    return {
        task: function (id, options) {
            if (!options) options = {};
            return $http.post(baseUrl + '/api/tasks/' + id, options).then(function (response) {
                console.log("task loaded its data:");
                console.log(response.data);
                return response.data;
            });
        },
        store: function (task) {
            return $http.post(baseUrl + '/api/tasks/store', task).then(function (response) {
                return response.data;
            });
        },
        update: function(task) {
            return $http.post(baseUrl + '/api/tasks/update', task).then(function (response) {
                return response.data;
            });
        },
        saveVisitPoint: function(visitPoint) {
            var url = baseUrl + '/api/tasks/saveVisitPoint';
            return $http({
                url: url,
                method: 'POST',
                data: visitPoint
            }).success(function (response) {
                return response;
            }).error(function (response) {
                return response;
            });
        }
    };
});