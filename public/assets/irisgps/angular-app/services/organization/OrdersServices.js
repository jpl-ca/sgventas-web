app.factory('ordersService', function($http) {
    return {
        getAll: function () {
            return $http.get(base_url +'/api/auth/organization/me/product-orders?includes=visitPoint').then(function (response) {
                return response.data;
            });
        }
        ,
        updateState: function(id, state) {
            return $http.put(baseUrl + '/api/auth/organization/me/product-orders/' + id, {status:state}).then(function (response) {
                return response.data;                
            });
        }
        ,
        getOrder: function (id) {
            return $http.get(base_url +'/api/auth/organization/me/product-orders/'+ id +'?fields=productOrderItems,visitPoint').then(function (response) {
                return response.data;
            });
        }
        ,
        searchOrders: function (searchTerm, searchType) {
            return $http.get(base_url +'/api/auth/organization/me/product-orders?includes=visitPoint&searchTerm='+ searchTerm +'&searchType='+ searchType).then(function (response) {
                console.log(response.data);
                return response.data;
            });
        }
    };
});