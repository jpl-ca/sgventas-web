'use strict';
var base_url=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var app = angular.module('app', []);

app.controller('OrdersController', ['$scope', '$http', 'ordersService', '$timeout', function($scope, $http, ordersService, $timeout)
    {
        var html = function(id) { return document.getElementById(id); };
        $scope.statesList = [
            {
                machineName: 'pending',
                label: 'Pendiente'
            },
            {
                machineName: 'confirmed',
                label: 'Confirmado',
            },
            {
                machineName: 'done',
                label: 'Hecho'
            },
            {
                machineName: 'canceled',
                label: 'Anulado'
            }
        ];


        $scope.getAll = function() {
            ordersService.getAll().then(function (response) {
                $scope.orders = response.data;                    
            }, function (response) {
                console.log(response);                
            });
        }
        $scope.getAll();

        $scope.activa_desactiva = function(id, state) {
          ordersService.updateState(id, state).then(function (response) {
                $scope.getAll();
                console.log(response);                      
            }, function (response) {
                console.log(response);                
            });   
        }
        $scope.visible = "all";
        $scope.$watch('searchType', function(value){
          if(value){
            if (value == 'status') {
                $scope.visible = "state";
            }else if (value == 'description') {
                $scope.visible = "text";
            }else if (value == 'all') {
                $scope.visible = "all";
            };
          }
         });
        $scope.searchOrders = function() {
            if ($scope.visible == "all") {
                $scope.getAll();
            }else{
                var searchTerm = html("searchTerm").value;
                $scope.orders = [];
                ordersService.searchOrders(searchTerm, $scope.searchType).then(function (response) {
                    $scope.orders = response.data;                    
                }, function (response) {
                    console.log(response.data);                
                });
            };
        }

        $scope.changeState = function (order) {
            $.confirm({
                title: 'Alert!',
                text: '¿Está seguro de cambiar de estado?',
                "confirm-button": "Sí, estoy seguro.",
                "cancel-button": "Cancelar.",
                confirm: function (){
                    $scope.saveStateOfOrder(order.id, order.status);
                },
                cancel: function () {
                    $scope.getAll();
                }
            });
        }

        $scope.saveStateOfOrder = function (id, state) {
            ordersService.updateState(id, state).then(function (response) {
                $scope.getAll();
            }, function (response) {
            });   
        }
    }]);

app.controller('ShowOrdersController', ['$scope', '$http', 'ordersService', '$timeout', function($scope, $http, ordersService, $timeout)
    {
        $scope.$watch('order_id', function(order_id){
          if(order_id){
            $scope.loadOrder(order_id);
          }
         });
        $scope.loadOrder = function(order_id) {
            ordersService.getOrder(order_id).then(function (response) {
                $scope.order = response.data;                    
            }, function (response) {
                console.log(response);                
            });
        }

        $scope.getTotal = function ()
        {
            var total = 0;
            for(var i in $scope.order.product_order_items) {
                var price = parseFloat($scope.order.product_order_items[i].price);
                var quantity = parseFloat($scope.order.product_order_items[i].quantity);
                var subtotal = price * quantity;
                $scope.order.product_order_items[i].subtotal = subtotal;
                total = total + subtotal;
            }
            return total;
        }
    }]);