var telInput = $("#phone"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");
   addressDropdown = $("#selected_country");
// initialise plugin
telInput.intlTelInput({
  initialCountry: "auto",
  geoIpLookup: function(callback) {
    $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
      var countryCode = (resp && resp.country) ? resp.country : "";
      callback(countryCode);
    });
  },
  utilsScript: baseUrl+"/assets/phone_validator/build/js/utils.js"
});

// listen to the telephone input for changes
telInput.on("countrychange", function(e, countryData) {
  addressDropdown.val(countryData.iso2);
});

// listen to the address dropdown for changes
addressDropdown.change(function() {
  telInput.intlTelInput("setCountry", $(this).val());
});

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      // validMsg.removeClass("hide");
    } else {
      telInput.addClass("error");
      // errorMsg.removeClass("hide");
      displayToastr('toast-bottom-right', 'error', "Ingrese un teléfono valido");
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);