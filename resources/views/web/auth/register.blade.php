@extends('layouts.web')

@section('head')
    {!! Html::style('assets/phone_validator/build/css/intlTelInput.css?1456859475147') !!}
@endsection

@section('content')
<style>
body{
background:url('/assets/irisgps/img-web/foto_portada_2.jpg')no-repeat left top;
background-size: cover;      
}
/*footer{
    position: absolute !important;
}*/
</style>
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-8 col-xs-1"></div>
        <div class="col-md-5 col-md-8 col-xs-10 login-register">
            <h4 class="tlogin">Regístrate</h4>
            <form method="POST" action="{{ url ('/register') }}" accept-charset="UTF-8" class="form-horizontal" role="form"><input name="_token" type="hidden" value="7Ba7MXbcrRFh4U7qUrAM47sNaXVwOkw0kWCoewW4">

                <div class="form-group">
                    <label for="Name" class="control-label col-md-4 l16">PAIS</label>

                    <div class="col-md-7">
                        {!! Form::select('pais', array('PE' => 'Perú', 'BO' => 'Bolivia'), old('pais'), ['class' => 'form-control', 'id' => 'input_pais']); !!}
                    </div>
                </div>
                
                <?php
                    $pais = old('pais');

                    $document = "RUC";

                    if ($pais == 'BO') {
                        $document = "NIT";    
                    }
                    
                    if ($errors->has('ruc')) {
                        foreach($errors->get('ruc') as $error) {
                            $errorRuc = str_replace("ruc", $document, $error);
                        }
                    }
                ?>
                <div class="form-group {{ $errors->has('ruc') ? "has-error" : '' }}">
                    <label id="document_type" for="Name" class="control-label col-md-4 l16">{{$document}}</label>

                    <div class="col-md-7">
                        <input class="form-control" autocomplete="off" name="ruc" type="text" value='{{old("ruc")}}'>
                        @if ($errors->has('ruc'))
                            @foreach($errors->get('ruc') as $error)
                            <span class="help-block">
                                <strong>{{$errorRuc}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('name') ? "has-error" : '' }}">
                    <label for="Name" class="control-label col-md-4 l16">Nombre</label>

                    <div class="col-md-7">
                        <input class="form-control" autocomplete="off" name="name" type="text" value='{{old("name")}}'>
                        @if ($errors->has('name'))
                            @foreach($errors->get('name') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('email') ? "has-error" : '' }}">
                    <label for="E-mail" class="control-label col-md-4 l16">Correo</label>

                    <div class="col-md-7">
                        <input class="form-control" autocomplete="off" name="email" type="text" value='{{old("email")}}'>
                        @if ($errors->has('email'))
                            @foreach($errors->get('email') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('password') ? "has-error" : '' }}">
                    <label for="Password" class="control-label col-md-4 l16">Clave</label>
                    <div class="col-md-7">
                        <input class="form-control" autocomplete="off" name="password" type="password" value="{{old('password')}}">
                        @if ($errors->has('password'))
                            @foreach($errors->get('password') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('password_confirmation') ? "has-error" : '' }}">
                    <label for="Password Confirmation" class="control-label col-md-4 l16">Confirmación de clave</label>

                    <div class="col-md-7">
                        <input class="form-control" autocomplete="off" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}">
                        @if ($errors->has('password_confirmation'))
                            @foreach($errors->get('password_confirmation') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('phone_number') ? "has-error" : '' }}">
                    <label for="phone_number" class="control-label col-md-4 l16">Teléfono</label>
                    <div class="col-md-7">
                        <input class="form-control" autocomplete="off" id="phone_number" name="phone_number" type="text" value="{{ old('phone_number', '+51') }}">
                        @if ($errors->has('phone_number'))
                            @foreach($errors->get('phone_number') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                
                
                <div class="form-group {{ $errors->has('category') ? "has-error" : '' }}">
                    <label for="category" class="control-label col-md-4 l16">Sector</label>
                    <div class="col-md-7">
                        {!! Form::select('category', $categories, old('category'), ['class' => 'form-control', 'id' => 'input_category']); !!}
                        @if ($errors->has('category'))
                            @foreach($errors->get('category') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('organization') ? "has-error" : '' }}">
                    <label for="Organization" class="control-label col-md-4 l16">Organización</label>

                    <div class="col-md-7">
                        <input class="form-control" id="organization" autocomplete="off" name="organization" type="text" value='{{old("organization")}}'>
                        @if ($errors->has('organization'))
                            @foreach($errors->get('organization') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('organization_id') ? "has-error" : '' }}">
                    <label for="Organización ID" class="control-label col-md-4 l16">Organización ID</label>

                    <div class="col-md-7">
                        <input class="form-control" id="organization_id" autocomplete="off" readonly="1" name="organization_id" type="text" value='{{old("organization_id")}}'>
                        @if ($errors->has('organization_id'))
                            @foreach($errors->get('organization_id') as $error)
                            <span class="help-block">
                                <strong>{{$error}}</strong>
                            </span>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-sgtask">
                            Crear Cuenta
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@section('javascript')
    {{ Html::script('assets/phone_validator/build/js/intlTelInput.js?1456859475147') }}
    <script>
        $(document).ready(function () {
            $("#phone_number").intlTelInput();
        } );
        $('#organization').on('input', function() {
            $('#organization_id').val(xxx($(this).val()));
        });

        function xxx(str) {
            str = str
                    .toLowerCase()
                    .replace(/\s+/g, '-')
                    .replace(/\.+/g, '')
                    .replace(/ñ/g, 'n')
                    .replace(/á/g, 'a')
                    .replace(/ä/g, 'a')
                    .replace(/â/g, 'a')
                    .replace(/é/g, 'e')
                    .replace(/ë/g, 'e')
                    .replace(/ê/g, 'e')
                    .replace(/í/g, 'i')
                    .replace(/ï/g, 'i')
                    .replace(/î/g, 'i')
                    .replace(/ó/g, 'o')
                    .replace(/ö/g, 'o')
                    .replace(/ô/g, 'o')
                    .replace(/ú/g, 'u')
                    .replace(/ü/g, 'u')
                    .replace(/û/g, 'u')
                    .replace(/[^a-zA-Z0-9\-]/g, '')
                    .replace(/\-+/g, '-')
                    /*
                    .replace(/[\.{}\[\]]+/g, '')
                    */
            return str;
        }

        $("#input_pais").on("change", function () {
            var pais = $(this).val();
            var document_type = 'RUC';

            switch (pais) {
                case 'PE':
                    document_type = 'RUC';
                    $('#phone_number').intlTelInput("setCountry", "pe");
                    break;
                case 'BO':
                    document_type = 'NIT';
                    $('#phone_number').intlTelInput("setCountry", "bo");
                    break;
            }

            $("#document_type").html(document_type);
        });
    </script>
@endsection