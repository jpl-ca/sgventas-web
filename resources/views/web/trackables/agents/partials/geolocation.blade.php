@if(!$agent->trackable->hasGeolocation())
<p class='text-center'>
	<span>Este agente no ha registrado su ubicación geográfica</span>
</p>
@endif
<div id="map" class="margin-bottom-20"
	@if($agent->trackable->hasGeolocation())
		data-lat="{{ $agent->trackable->lastGeolocation()->lat }}" data-lng="{{ $agent->trackable->lastGeolocation()->lng }}"
	@endif
></div>
<div class="hidden map-markers">
    @if($agent->trackable->hasGeolocation())
        <div data-role="marker" data-lat="{{ $agent->trackable->lastGeolocation()->lat }}" data-lng="{{ $agent->trackable->lastGeolocation()->lng }}" data-title="{{ $agent->full_name }}" data-status="{{ $agent->trackable->state }}" data-icon="{{ $agent->trackable->getIcon() }}" data-info-window="{{ $agent->trackable->getInfoWindow() }}"></div>
    @endif
</div>