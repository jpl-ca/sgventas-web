@extends('layouts.web')

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2  card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Editando Agente #{{$agent->authentication_code}}
                 <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a></h2>


                {!! Form::fhOpen(['action' => ['Web\TrackableController@updateAgent', $agent], 'method' => 'POST']) !!}

                {!! Form::token() !!}

                {!! Form::fhText('first_name', 'Nombres', $agent->first_name, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('last_name', 'Apellidos', $agent->last_name, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('authentication_code', 'Código de autenticación', $agent->authentication_code, ['autocomplete' => 'off']) !!}

                {!! Form::fhPassword('password', 'Clave de autenticación') !!}

                {!! Form::fhSubmit('Actualizar agente', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
