@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Html::pageHeader("Nuevo vehículo") !!}

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url()->previous() }}" class="btn btn-default pull-left margin-bottom-20">Ir atrás</a>
                    </div>
                </div>

                {!! Form::fhOpen(['action' => 'Web\TrackableController@storeVehicle', 'method' => 'POST']) !!}

                    {!! Form::token() !!}

                    {!! Form::fhText('plate', 'Placa', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('brand', 'Marca', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('model', 'Modelo', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('color', 'Color', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('manufacture_year', 'Año de fabricación', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('gas_consumption_rate', 'Consumo de gas', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhPassword('password', 'Clave de autenticación') !!}

                    {!! Form::fhSubmit('Crear nuevo vehículo', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
