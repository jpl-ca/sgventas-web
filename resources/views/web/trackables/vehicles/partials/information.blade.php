<div class="row margin-bottom-20">
    <div class="col-md-12">

        {!! Form::fhText('plate', 'Placa', $vehicle->plate, ['readonly' => true]) !!}

        {!! Form::fhText('brand', 'Marca', $vehicle->brand, ['readonly' => true]) !!}

        {!! Form::fhText('model', 'Modelo', $vehicle->model, ['readonly' => true]) !!}

        {!! Form::fhText('color', 'Color', $vehicle->color, ['readonly' => true]) !!}

        {!! Form::fhText('manufacture_year', 'Año de fabricación', $vehicle->manufacture_year, ['readonly' => true]) !!}

        {!! Form::fhText('gas_consumption_rate', 'Tarifa de consumo de gas', $vehicle->gas_consumption_rate, ['readonly' => true]) !!}

    </div>
</div>