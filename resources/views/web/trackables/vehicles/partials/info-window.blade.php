<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $vehicle->plate }}</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="description-row">
                    <label>State:</label>
                    <p>{{ $vehicle->trackable->state }}</p>
                </div>
                <div class="description-row">
                    <label>Last geolocation date:</label>
                    <p>{{ $vehicle->trackable->lastGeolocationDateTime() }}</p>
                </div>
                <div class="description-row">
                    <label>Transmission frequency:</label>
                    <p>{{ $vehicle->trackable->transmissionFrequency() }} seconds</p>
                </div>
            </div>
        </div>
    </div>
</div>