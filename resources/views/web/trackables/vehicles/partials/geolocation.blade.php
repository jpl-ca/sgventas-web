<div id="map" class="margin-bottom-20"></div>
<div class="hidden map-markers">
    @if($vehicle->trackable->hasGeolocation())
        <div data-role="marker" data-lat="{{ $vehicle->trackable->lastGeolocation()->lat }}" data-lng="{{ $vehicle->trackable->lastGeolocation()->lng }}" data-title="{{ $vehicle->plate }}" data-status="{{ $vehicle->trackable->state }}" data-icon="{{ $vehicle->trackable->getIcon() }}" data-info-window="{{ $vehicle->trackable->getInfoWindow() }}"></div>
    @endif
</div>