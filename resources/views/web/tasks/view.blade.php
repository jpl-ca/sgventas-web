@extends('layouts.web')

@section('content')
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-8 col-md-offset-2 card-box padding-30">
                {!! Html::pageHeader("Ruta #$task->id") !!}
                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url()->previous() }}" class="btn btn-default pull-left margin-bottom-20">Ir Atrás</a>
                    </div>
                </div>

                <div class="row margin-bottom-20">
                    <div class="col-md-12">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Información</a></li>
                            <li role="presentation"><a href="#visit_points" aria-controls="visit_points" role="tab" data-toggle="tab">Puntos de Visita</a></li>
                            <li role="presentation"><a href="#tracking" aria-controls="tracking" role="tab" data-toggle="tab">Rastreo</a></li>
                        </ul>

                    </div>
                </div>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="info">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('web.tasks.partials.information')
                                <a href="{{ action('Web\TaskController@edit', $task) }}" class="btn btn-primary margin-bottom-20">Editar</a>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="visit_points">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                @include('web.tasks.partials.visit-points')
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="tracking">
                        <div class="row">
                            <div class="col-md-12">
                                @include('web.tasks.partials.geolocation')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    @include('web.includes.google-maps')
    {!! Html::script('assets/irisgps/js/maps.js') !!}
    {!! Html::script('assets/irisgps/js/geolocation.js') !!}

    <script type="text/javascript">
        $(document).ready(function() {
            $('a[data-toggle="tab"][href="#tracking"]').on('shown.bs.tab', function (e) {
                initGeolocation();
            });
        });
    </script>
@endsection