<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    @foreach($task->visitPoints as $visitPoint)
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    {{ $visitPoint->name }}
                    <span class="label label-task-{{ $visitPoint->getStateLabel() }}">
                        {{ $visitPoint->state->name . ' ' . $visitPoint->checklist_done_items . '/' . $visitPoint->checklist_items }}
                    </span>
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                @foreach($visitPoint->checklist as $item)
                    <div class="row">
                        <div class="col-md-12">
                            @if($item->checked)
                                <span class="fa fa-check-square-o text-success" aria-hidden="true"></span>
                            @else
                                <span class="fa fa-square-o text-default" aria-hidden="true"></span>
                            @endif
                            {{ $item->description }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    @endforeach
</div>