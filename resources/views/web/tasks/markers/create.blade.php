@extends('layouts.web')

@section('head')
    {{ Html::script('assets/phone_validator/examples/js/prism.js?1456859475147') }}
    {!! Html::script('assets/angular/angular.min.js') !!}
    {!! Html::style('assets/phone_validator/build/css/intlTelInput.css?1456859475147') !!}
@endsection

@section('body-tag')
    ng-app="irisGpsApp"
@endsection


@section('content')
<style>
    #error-msg {
  color: red;
}
#valid-msg {
  color: #00C900;
}
input.error {
  border: 1px solid #FF7C7C;
}
</style>
    <div class="container-fluid" ng-controller="markerController">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Nuevo Cliente
                <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a></h2>

                <div class="row margin-bottom-20">
                    <div class="col-md-6">
                        <p>
                            <strong>Click sobre el mapa para agregar la ubicación de un cliente. Las coordenadas se agregarán automáticamente</strong>
                        </p>
                        <form id="markerForm">
                            @include('web.tasks.markers.partials.creation-form')
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div id="map-new-marker"></div>
                    </div>
                </div>

                <div class="row margin-bottom-20">
                    <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-primary" ng-disabled="markerForm.$invalid || isSubmitted" ng-click="addNewMarker()">
                            <i class="fa fa-check"></i>
                            Crear nuevo cliente
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('javascript')
    @include('web.includes.google-maps')
    {{ Html::script('assets/phone_validator/build/js/intlTelInput.js?1456859475147') }}
    {{ Html::script('assets/phone_validator/examples/js/isValidNumber.js?1456859475147') }}
    {{ Html::script('assets/irisgps/angular-app/angular.irisgps.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/MarkerService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/AgentService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/VehicleService.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/TaskService.js') }}
    <script>

        irisGpsApp.controller('markerController', ['$scope', '$compile', '$window', 'MarkerService', function($scope, $compile, $window, MarkerService) {

            $scope.loadCountries = function(){
                MarkerService.countries().then(function(response){
                    $scope.countries = response;
                    console.log($scope.countries);
                })
            }
            $scope.loadCountries();
            $scope.$watch('selected_country', function(country){
                if(country){
                    $scope.centermap = {lat:country.lat, lng:country.lng};

                    $('#new-marker-name').focus();
                    mapNewMarker = initMap2('map-new-marker'); 
                    setMapStyles(mapNewMarker, defaultMapStyle());
                    // mapNewMarker.setZoom(12);

                    var newMarker = validateLoc(mapNewMarker);

                    google.maps.event.addListener(mapNewMarker, 'click', function(event) {
                    newMarker.setPosition(event.latLng);
                    newMarker.setMap(mapNewMarker);
                    newMarker.setAnimation(google.maps.Animation.DROP);

                    $scope.newMarker.latitude = event.latLng.lat();
                    $scope.newMarker.longitude = event.latLng.lng();
                    $scope.$apply();

                    });
                }               
            });

            function initMap2(elementId, mapOptions) {
                var map;
                console.log($scope.centermap);
                if (mapOptions == null) {
                    mapOptions = {
                        zoom: 5,
                        center: $scope.centermap,
                        streetViewControl: false,
                        mapTypeControl: false   ,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                }
                var map = new google.maps.Map(document.getElementById(elementId), mapOptions)
                return map;
                
            }

            $scope.newMarker = {};
            $scope.isSubmitted = false;

            $('#new-marker-name').focus();
            mapNewMarker = initMap('map-new-marker');
            setMapStyles(mapNewMarker, defaultMapStyle());
            mapNewMarker.setZoom(12);

            var newMarker = validateLoc(mapNewMarker);

            google.maps.event.addListener(mapNewMarker, 'click', function(event) {
                newMarker.setPosition(event.latLng);
                newMarker.setMap(mapNewMarker);
                newMarker.setAnimation(google.maps.Animation.DROP);

                $scope.newMarker.latitude = event.latLng.lat();
                $scope.newMarker.longitude = event.latLng.lng();
                $scope.$apply();

            });
            
            $scope.addNewMarker = function() {                
                if ($.trim(telInput.val()) == "") {
                    displayToastr('toast-bottom-right', 'error', "Complete el teléfono ");
                }else if ($.trim(telInput.val())) {
                    if (telInput.intlTelInput("isValidNumber")) {
                        $scope.isSubmitted = true;
                        MarkerService.store($scope.newMarker).then(function(response){
                            displayToastr('toast-bottom-right', 'success', response);
                            $scope.newMarker = {};
                            $window.location.replace("/markers");
                        }, function(response){
                            var errors = response.data;
                            $.each(errors, function ( index, value ) {
                                displayToastr('toast-bottom-right', 'error', value);
                                $scope.isSubmitted = false;
                            });
                        });
                    } else {
                      telInput.addClass("error");
                      displayToastr('toast-bottom-right', 'error', "Ingrese un teléfono valido");
                    }
                }
                // $scope.isSubmitted = true;
                // MarkerService.store($scope.newMarker).then(function(response){
                //     displayToastr('toast-bottom-right', 'success', response);
                //     $scope.newMarker = {};
                //     $window.location.replace("/markers");
                // }, function(response){
                //     var errors = response.data;
                //     $.each(errors, function ( index, value ) {
                //         displayToastr('toast-bottom-right', 'error', value);
                //         $scope.isSubmitted = false;
                //     });
                // });
            }

            function validateLoc(map)
            {
                var lat = $.trim($("#new-marker-lat").val());
                var lng = $.trim($("#new-marker-lng").val());
                if(lat.length>0 && lng.length>0)
                {
                    var coordinate = new google.maps.LatLng(lat, lng);
                    var marker = new google.maps.Marker({
                        position: coordinate,
                        map: map,
                        animation: google.maps.Animation.DROP
                    });
                }else{
                    var marker = new google.maps.Marker({});
                }
                return marker;
            }
        }]);

    </script>    
    {{ Html::script('assets/irisgps/js/maps.js') }}
@endsection