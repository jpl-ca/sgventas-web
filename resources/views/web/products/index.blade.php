@extends('layouts.web')

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
<style>
    footer{
        margin-top: 0px;
    }
    .web-container {
        height: 633px;
    }
</style>
    <div class="container-fluid">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Productos</h2>
                <div ng-controller="ProductController">
                	<nav class="navbar navbar-default">
					  <div class="col-md-12">
					    <form class="navbar-form navbar-left">					        
					        <div class="form-group">
					        	<select class="form-control" id="searchType" ng-model="searchType">
					        		<option value="name" selected="selected">Nombre</option>
					        		<option value="price">Precio</option>
					        		<option value="category">Categoria</option>
					        		<option value="brand">Marca</option>
					        		<option value="all">Todos</option>
					        	</select>
					        </div>
					        <div class="form-group" ng-if="visible == 'name'">
					          	<input class="form-control" placeholder="Ingrese producto a buscar" 
					          	id="searchTerm" type="text" required>
					        </div>
					        <div class="form-group" ng-if="visible == 'price'">
					          	<input class="form-control" placeholder="Ingrese texto a buscar" 
					          	id="searchTerm" type="number" required>
					        </div>
					        <div class="form-group" ng-if="visible == 'category'">
					          	<input class="form-control" placeholder="Ingrese texto a buscar" 
					          	id="searchTerm" type="text" required>
					        </div>
					        <div class="form-group" ng-if="visible == 'brand'">
					          	<input class="form-control" placeholder="Ingrese texto a buscar" 
					          	id="searchTerm" type="text" required>
					        </div>
					        <button type="submit" class="btn btn-default" ng-click="searchProducts()">Buscar</button>
					    </form>
					    <div class="navbar-right">
					    	<a href="{{ url('/api/auth/organization/me/products?format=xls') }}" class="btn btn-default margin-top-15" ng-click="searchOrders()">
					    	<i class="fa fa-download"></i> Exportar</a>
					    </div>
					    <div class="text-center margin-top-15">
                        <a href="{{ action('Web\ProductController@create') }}" class="btn btn-primary">
                            <i class="fa fa-btn fa-plus"></i>
                            Nuevo Producto
                        </a>    
                        <a  href="#" data-toggle="modal" data-target="#newMarkerModal" class="btn btn-warning">
                            <i class="fa fa-btn fa-upload"></i>
                            Importar Productos
                        </a> 
                    </div>
					  </div>
					</nav>
	                <table class="table table-striped table-hover ">
					  <thead>
					    <tr>
					      <th>#</th>
					      <th>Nombre</th>
					      <th>Precio</th>
					      <th>Stock</th>
					      <th>Categoria</th>
					      <th>Marca</th>
					      <th>Descripción</th>
					      <th>Acción</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr ng-repeat="product in products track by $index">
					      <td>@{{$index + 1}}</td>
					      <td>@{{product.name}}</td>
					      <td>@{{ product.currency_symbol }} @{{product.price}}</td>
						  <td>@{{product.stock}}</td>
						  <td>@{{product.category}}</td>
						  <td>@{{product.brand}}</td>
						  <th>@{{product.brief_description}}</th>
					      <td>
					      	<a  href="{{ url('products/') }}/@{{product.id}}" 
                            	data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                             	<i class="fa fa-eye"></i>
                            </a>					      	
                            <a  href="{{ url('products/') }}/@{{product.id}}/edit/" 
                            	data-original-title="Ver detalle" class="btn btn-xs" data-toggle="tooltip" data-placement="top">
                             	<i class="fa fa-edit"></i>
                            </a>
                            <a  ng-click="deleteProduct(product.id)"
                            	 data-original-title="Eliminar" class="btn btn-xs" data-toggle="tooltip" data-placement="top" >
                             	<span class="fa fa-trash" aria-hidden="true"></span>
                            </a>  
					      </td>
					    </tr>
					    <tr>
					    	<td></td>
					    	<td></td>
					    	<td></td>
					    	<td></td>
					    	<td class="text-center" ng-hide="products.length">
					    		No hay elementos...
					    	</td>
					    	<td></td>
					    	<td></td>
					    	<td></td>
					    </tr>
					  </tbody>					  
					</table> 
	               <!--  <ul class="pagination">
		                <li class="disabled"><a href="#">«</a></li>
		                <li class="active"><a href="#">1</a></li>
		                <li class="disabled"><a href="#">»</a></li>
		            </ul> -->
		            <ul class="pagination">
                        <li ng-class="{active:0}"><a href="#" ng-click="page_anterior()">«</a></li>
                        <li class="active"><a>@{{current_page}}</a></li>
                        <li><a href="#" ng-click="page_siguiente()">»</a></li>
                    </ul> 
		            <div class="modal fade" id="newMarkerModal" tabindex="-1" role="dialog" aria-labelledby="newMarkerModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <!-- <h4 class="modal-title" id="newMarkerModalLabel">Importar Productos</h4> -->
                                    <h4 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Importar Productos </h4>
                                </div>
                                <div class="modal-body">
                                    <form name="myForm" class="ng-pristine ng-invalid ng-invalid-required form-horizontal">
                                        <div class="row">
                                            <div class="col-md-12">	
                                            	<a class='pull-right' target="_blank" href="{{url('/docs/products-example.csv')}}"><i class='fa fa-file'></i> Descargar archivo de ejemplo</a>
                                            	<h5 for="">Archivo .csv</h5>
									              <input type="file" ngf-select=""  file-reader="fileContent" ng-model="picFile" name="archivo" required="" 
									              class="ng-pristine ng-invalid ng-invalid-required">
									              <br>
									              <hr>
    											<table class="table table-striped table-hover ">
													  <thead>
													    <tr>
													      <th>Nombre</th>
													      <th>Descripción</th>
													      <th>Stock</th>
													      <th>Precio</th>
													      <th>Marca</th>
													      <th>Categoria</th>
													      <th>Modelo</th>
													    </tr>
													  </thead>
													  <tbody>
													    <tr ng-repeat="prod in fileContent">
													      <td>@{{prod.name}}</td>
													      <td>@{{prod.description}}</td>
														  <td>@{{prod.stock}}</td>
														  <td>@{{prod.price}}</td>
														  <td>@{{prod.brand}}</td>
														  <td>@{{prod.category}}</td>
														  <td>@{{prod.model}}</td>
													    </tr>
													  </tbody>
												</table> 
                                            </div>
                                        </div>
                                    
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-primary" ng-click="importcsv(picFile)">
                                        <i class="fa fa-check"></i>
                                        Importar Productos
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {!! Html::script('assets/angular/angular.min.js') !!}
    {{ Html::script('assets/irisgps/angular-app/controllers/organization/productsCtrl.js')}}
	{{ Html::script('assets/irisgps/angular-app/services/organization/ProductsServices.js')}}
	{{ Html::script('assets/bower_components/ng-file-upload/ng-file-upload.min.js')}}
	{{ Html::script('assets/bower_components/ng-file-upload-shim/ng-file-upload.min.js')}}
@endsection