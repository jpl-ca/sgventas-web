@extends('layouts.web-admin')

@section('head')
    {!! Html::style('assets/bower_components/angular-chart.js/dist/angular-chart.css') !!}
@endsection

@section('body-tag')
    ng-app="app"
@endsection

@section('content')
<style>
    #chartdiv {
    width   : 100%;
    height  : 600px;
}                                   
                                        
</style>
    <div class="container-fluid" ng-controller="ReportsController2">
        <div class="container  margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <h2 class="page-header"><img src="{{ url('assets/irisgps/img-web/flecha.png')}}" alt="">Reporte de Visitas</h2>
                <div>
                    <nav class="navbar navbar-default">
                      <div class="container">
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <label for="">Desde: </label>
                                <input class="form-control" id="start_date" placeholder="YYYY-MM-DD" autocomplete="off" ng-model="start_date" required="1" name="start_date" type="text">
                            </div>
                            <div class="form-group">
                                <label for="">Hasta: </label>
                                <input class="form-control" id="end_date" placeholder="YYYY-MM-DD" autocomplete="off" ng-model="end_date" required="1" name="end_date" type="text">
                            </div>
                            <div class="form-group">
                                <label for="">Organización: </label>
                                <select class="form-control" id="selected_org" name="org"  ng-model="selected_org"
                                 ng-init="selected_org = orgs[0]" 
                                 ng-options="org as org.name for org in orgs track by org.name">
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default" ng-click="getVisitsforOrg()">Buscar</button>
                        </form>
                      </div>
                    </nav>
                    <br> 
                    <p class="text-center tselect" ng-if="data == ''">No se encontraron registros.</p>  
                    <p class="text-center tselect" ng-if="data == undefined">Selecciones un rango de fechas.</p>
                     <div id="chartdiv"></div>
                     
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    {!! Html::script('assets/angular/angular.min.js') !!}
        {{ Html::script('assets/amcharts/amcharts.js') }}
    {{ Html::script('assets/amcharts/serial.js') }}
    {{ Html::script('assets/irisgps/angular-app/controllers/admin/reportsCtrl.js') }}
    {{ Html::script('assets/irisgps/angular-app/services/admin/reportsService.js') }}
    {!! Html::style('assets/amcharts/plugins/export/export.css') !!}
    {!! Html::script('assets/amcharts/plugins/export/export3.js') !!}
    {!! Html::script('assets/amcharts/plugins/export/examples/export.config.default.js') !!}
@endsection