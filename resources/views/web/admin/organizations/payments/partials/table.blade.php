<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Monto</th>
        <th>Inicio Suscripción</th>
        <th>Fin Suscripción</th>
        <th>Información Adicional</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @forelse($items as $item)
        <tr>
            <th>{{ $item->id  }}</th>
            <td class='text-left'>{{ $item->amount }}</td>
            <td class='text-left'>{{ $item->subscription_start_date }}</td>
            <td class='text-left'>{{ $item->subscription_end_date }}</td>
            <td class='text-left'>{{ $item->additional_information }}</td>
            <td class="text-center">
                <a data-original-title="ver"
                    href="{{ action('Web\Admin\OrganizationPaymentController@view',
                        [
                            'organization_id' => $item->organization_id,
                            'paymentId' => $item->id
                        ]) }}"
                    class="btn btn-xs"
                    data-toggle="tooltip"
                    data-placement="top">

                    <span class="fa fa-eye" aria-hidden="true"></span>
                </a>

                <a data-original-title="editar" href="{{ action('Web\Admin\OrganizationPaymentController@edit', 
                    [
                        'organization_id' => $item->organization_id,
                        'paymentId' => $item->id
                     ]) }}" class="btn btn-xs" data-toggle="tooltip" data-placement="top">

                    <span class="fa fa-pencil" aria-hidden="true"></span>
                </a>

                <a data-original-title="eliminar"
                   href="{{ action('Web\Admin\OrganizationPaymentController@destroy', ['organization_id' => $item->organization_id, 'paymentId' => $item->id]) }}"
                   class="confirm-post btn btn-xs"
                   data-text="¿Estás seguro de eliminar esto?"
                   data-confirm-button="Sí, estoy seguro"
                   data-cancel-button="No estoy seguro"
                   data-toggle="tooltip"
                   data-placement="top">
                    <span class="fa fa-trash" aria-hidden="true"></span>
                </a>

            </td>
        </tr>
    @empty
        <tr class="text-center">
            <td colspan="5">No hay registros para mostrar...</td>
        </tr>
    @endforelse
    </tbody>
</table>