@extends('layouts.web-admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Html::pageHeader("Nuevo usuario") !!}

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{ url()->previous() }}" class="btn btn-default pull-left margin-bottom-20">Ir atrás</a>
                    </div>
                </div>

                {!! Form::fhOpen(['action' => 'Web\UserController@store', 'method' => 'POST']) !!}

                    {!! Form::token() !!}

                    {!! Form::fhText('name', 'Nombre', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhText('email', 'Correo', null, ['autocomplete' => 'off']) !!}

                    {!! Form::fhPassword('password', 'Clave de autenticación') !!}

                    {!! Form::fhSubmit('Crear nuevo usuario', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
