@extends('layouts.web-admin')

@section('content')
<style>
  footer{
    position: absolute !important;
  }
</style>
    <div class="container">
        <div class="container margin-top-40">
            <div class="col-md-12 card-box padding-30">
                <ol class="breadcrumb margin-top-10">
                  <li><a href="/admin">Inicio</a></li>
                  <li><a href="/admin/organizations">Organizaciones</a></li>
                  <li><a href="/admin/organizations/{{ $organization->id }}">{{ $organization->name}}</a></li>
                  <li><a href="/admin/organizations/{{ $organization->id }}/users">Usuarios</a></li>
                  <li class='active'>Edición de usuario</li>
                </ol>
                
                <h2 class="page-header">
                  <img src="{{ url('assets/irisgps/img-web/flecha.png') }}" alt="">
                  Edición de usuario
                  <a href="{{ url()->previous() }}" class="btn btn-default pull-right margin-bottom-20">Ir atrás</a>
                </h2>

                {!! Form::open(['url' => "/admin/organizations/{$user->organization->id}/users/edit/{$user->id}", 'method' => 'POST']) !!}

                {!! Form::token() !!}
                
                @if(isset($nextUrl) && $nextUrl)
                {!! Form::hidden('nextUrl', $nextUrl) !!}
                @endif

                {!! Form::fhText('name', 'Nombre', $user->name, ['autocomplete' => 'off']) !!}

                {!! Form::fhText('email', 'Correo', $user->email, ['autocomplete' => 'off']) !!}
                
                {!! Form::fhPassword('password', 'Clave de autenticación') !!}

                {!! Form::fhSubmit('Actualizar usuario', 'primary', 'check') !!}

                {!! Form::fhClose() !!}

            </div>
        </div>
    </div>
@endsection
