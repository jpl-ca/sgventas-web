<?php

namespace IrisGPS\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use IrisGPS\Agent;
use IrisGPS\Marker;
use IrisGPS\Task;
use IrisGPS\Trackable;
use IrisGPS\User;
use IrisGPS\Vehicle;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'IrisGPS\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
        $router->model('userModel', User::class);
        $router->model('agentModel', Agent::class);
        $router->model('vehicleModel', Vehicle::class);
        $router->model('trackableModel', Trackable::class);
        $router->model('taskModel', Task::class);
        $router->model('markerModel', Marker::class);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
