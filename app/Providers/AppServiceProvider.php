<?php

namespace IrisGPS\Providers;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\ServiceProvider;

use Collective\Html\HtmlFacade as Html;

use IrisGPS\Organization;

use IrisGPS\Privilege;

use IrisGPS\User;

use IrisGPS\Agent;

use IrisGPS\ChecklistItem;

use IrisGPS\Marker;

use IrisGPS\Task;

use IrisGPS\Vehicle;

use IrisGPS\VisitState;

use Carbon\Carbon;

use Subscriber;

use IrisGPS\Suggestion;

use IrisGPS\OrganizationPayment;

use IrisGPS\TasksVisitPoint;

use IrisGPS\ProductOrder;

use IrisGPS\ProductOrderItem;

use IrisGPS\Observers\OrganizationPaymentObserver;

use IrisGPS\Observers\TasksVisitPointObserver;

use IrisGPS\Observers\ProductOrderObserver;

use IrisGPS\Observers\ProductOrderItemObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Html::macro('pageHeader', function($title = 'Page Title')
        {
            return "<h1 class=\"page-header\">$title</h1>";
        });

        Validator::extend('can_be_done_by', function($attribute, $value, $parameters, $validator) {
            $task = Task::where('id', $value)
                ->where('taskable_id', $parameters[0])
                ->where('taskable_type', $parameters[1])
                ->get();
            return !$task->isEmpty();
        });

        Validator::extend('docNumberOfOrganization', function($attribute, $value, $parameters, $validator) {

            $listOfFactors = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];

            //Checking RUC
            if (isset($value) && strlen($value) == 11) {
                $sum = 0;
                $lastDigit = (int)$value[10];

                for ($i = 0; $i < 10; $i++) {
                    $sum += (int)$value[$i] * $listOfFactors[$i];
                }

                $residue = $sum % 11;

                if ($residue < 10) {
                    $keyNumber = 11 - $residue;    
                } else {
                    $keyNumber = $keyNumber % 10;     
                }

                if ($lastDigit == $keyNumber) {
                    return true;
                }
            }

            //Checking NIT
            return preg_match('/^([0-9]{4}\.[0-9]{4}\.[0-9]{4}-[0-9]{1})$/', $value);
        });

        User::creating(function ($user) {
            if (auth('web')->check()) {
                $user->organization_id = auth('web')->user()->organization_id;
            } else {
                $user->user_type_id = 2;
            }
            return true;
        });

        User::created(function ($user) {
            if ($user->user_type_id = 1) {
                $privileges = Privilege::all();
                $userPrivileges = [];
                $date = Carbon::create()->toDateTimeString();
                foreach ($privileges as $privilege) {
                    array_push($userPrivileges, [
                        'user_id' => $user->id,
                        'privilege_id' => $privilege->id,
                        'created_at' => $date,
                        'updated_at' => $date
                    ]);
                }
                $db = DB::table('user_privileges')->insert($userPrivileges);
            }
            return true;
        });

        Agent::creating(function ($agent) {
            $agent->organization_id = auth('web')->user()->organization_id;
            return true;
        });

        Agent::created(function ($agent) {
            $tackable = $agent->trackable()->create([]);
            return true;
        });

        Agent::deleting(function ($agent) {
            $tackable = $agent->trackable->delete();
            return true;
        });

        Vehicle::creating(function ($vehicle) {
            if (auth('web')->guest()) {
                return false;
            }
            $vehicle->organization_id = auth('web')->user()->organization_id;
            return true;
        });

        Vehicle::created(function ($vehicle) {
            $tackable = $vehicle->trackable()->create([]);
            return true;
        });

        Vehicle::deleting(function ($agent) {
            $tackable = $agent->trackable->delete();
            return true;
        });

        Marker::creating(function ($marker) {
            if (auth('web')->guest()) {
                return false;
            }
            $marker->organization_id = auth('web')->user()->organization_id;
            return true;
        });

        Marker::updated(function ($marker) {
            DB::table('tasks_visit_points')
                ->where('marker_id', $marker->id)
                ->update([
                    'name' => $marker->name,
                    'address' => $marker->address,
                    'phone' => $marker->phone,
                    'reference' => $marker->reference,
                    'lat' => $marker->lat,
                    'lng' => $marker->lng
                ]);
            return true;
        });

        Task::creating(function ($task) {
            if (auth('web')->guest()) {
                return false;
            }
            $task->organization_id = auth('web')->user()->organization_id;
            return true;
        });

        ChecklistItem::updated(function ($listItem) {
            if($listItem->checked) {
                $taskVisitPoint = $listItem->taskVisitPoint;
                if ($taskVisitPoint->visit_state_id == VisitState::STATE_SCHEDULED
                    && $taskVisitPoint->checklist_done_items == $taskVisitPoint->checklist_items) {
                    $taskVisitPoint->visit_state_id = VisitState::STATE_DONE;
                    $taskVisitPoint->save();
                }
            } else {
                $taskVisitPoint = $listItem->taskVisitPoint;
                if ($taskVisitPoint->visit_state_id == VisitState::STATE_DONE
                    && $taskVisitPoint->checklist_done_items < $taskVisitPoint->checklist_items) {
                    $taskVisitPoint->visit_state_id = VisitState::STATE_SCHEDULED;
                    $taskVisitPoint->save();
                }
            }
        });

        Organization::created(function ($organization) {
            $basicPlan = Subscriber::plans()->find('basic');
            Subscriber::subscribe($organization, $basicPlan);
        });

        OrganizationPayment::observe(new OrganizationPaymentObserver);
        TasksVisitPoint::observe(new TasksVisitPointObserver);
        ProductOrder::observe(new ProductOrderObserver);
        ProductOrderItem::observe(new ProductOrderItemObserver);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
