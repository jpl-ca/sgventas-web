<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

class TrackableType extends Model
{
    const AGENT = 'IrisGPS\Agent';
    const VEHICLE = 'IrisGPS\Vehicle';
}
