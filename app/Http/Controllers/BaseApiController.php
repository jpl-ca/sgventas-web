<?php

namespace IrisGPS\Http\Controllers;

use Illuminate\Http\Request;

use Exception;

use IrisGPS\BaseApiModel;

use Maatwebsite\Excel\Facades\Excel;

trait BaseApiController
{
	protected $classReference = null;

	public function __construct ()
	{
        if (!isset($this->classReference)) {
            throw new Exception("classReference property is not setted in " . self::class);
        }
	}

    public function index(Request $request)
    {
        // Setting initial variables
        $classReference = $this->classReference;
        $format = $request->format ? $request->format : 'json';
        $options = [];
    	$options['page'] = $request->page ? $request->page : 1;
        $options['startDate'] = $request->startDate ? $request->startDate : null;
        $options['endDate'] = $request->endDate ? $request->endDate : null;
    	$options['searchTerm'] = $request->searchTerm ? $request->searchTerm : null;
        $options['searchType'] = $request->searchType ? $request->searchType : null;
        $options['filters'] = $request->filters ? $request->filters : null;

        $items = BaseApiModel::searchItemsQuery($classReference, $options);

        $items = $items->paginate();

        // Building Response
        switch ($format) {
            case 'xls':
                return $this->buildXlsFile($items)->download();

            case 'json':
            default:
                //Building JSON
                $json = $items;

                return response()->json($json);
                break;    
        }
        
    }

    public function create(Request $request)
    {
        $classReference = $this->classReference;
        $item = $classReference::create($request->all());

        //Building Json
        $json = [
            'message' => 'Elemento creado exitosamente',
            'data' => $item
        ];

        return response()->json($json);
    }

    public function show(Request $request, $id)
    {
        $classReference = $this->classReference;
        $item = $classReference::find($id);
        $fields = $request->fields ? $request->fields : null;

        // Validating 
        if (!$item) {
            $errors = [
                'model' => 'Elemento no encontrado'
            ];
        }

        if (isset($errors)) {
            $json = [
                'message' => 'Error',
                'errors' => $errors
            ];

            return response()->json($json, 422);
        }

        if (isset($fields)) {
            $fields = explode(',', $fields);
            foreach ($fields as $key => $_value) {
                $fields[$key] = trim($_value);
            }
            
            foreach ($fields as $key => $_value) {
                $item->load($_value);
            }

        }


        //Building JSON
        $json = [
            'message' => 'Elemento cargado exitosamente',
            'data' => $item
        ];

        return response()->json($json);
    }


    public function update(Request $request, $id)
    {
        $classReference = $this->classReference;
        $item = $classReference::find($id);

        // Validating 
        if (!$item) {
            $errors = [
                'model' => 'Elemento no encontrado'
            ];
        }

        if (isset($errors)) {
            $json = [
                'message' => 'Error',
                'errors' => $errors
            ];

            return response()->json($json, 422);
        }

        $item->fill($request->all());
        $item->save();

        //Building JSON
        $json = [
            'message' => 'Elemento encontrado exitosamente',
            'data' => $item
        ];

        return response()->json($json);
    }

    public function destroy(Request $request, $id)
    {
        $classReference = $this->classReference;
        $item = $classReference::find($id);

        // Validating 
        if (!$item) {
            $errors = [
                'model' => 'Elemento no encontrado'
            ];
        }

        if (isset($errors)) {
            $json = [
                'message' => 'Error',
                'errors' => $errors
            ];

            return response()->json($json, 422);
        }

        $item->delete();

        //Building JSON
        $json = [
            'message' => 'Elemento eliminado exitosamente',
        ];

        return response()->json($json);
    }

    protected function buildXlsFile($query)
    {
        $items = $query;

        $xls = Excel::create('Archivo', function($excel) use ($items) {
            $excel->sheet('Archivo', function($sheet) use ($items) {
                if (count($items) > 0) {
                    $sheet->row(1, array_keys($items[0]->toArray()));
                    $i = 2;
                    foreach ($items as $key => $value) {
                        $sheet->row($i, $value->toArray());
                        $i++;
                    }
                }
            });
        });

        return $xls;
    }
}
