<?php

namespace IrisGPS\Http\Controllers\Api\Organization\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\Product;

use IrisGPS\Report;

use Maatwebsite\Excel\Facades\Excel;

use Validator;

use Carbon\Carbon;

class ReportController extends Controller
{
   public function totalSalesMadeByEachAgent(Request $request)
   {
      // Settings initial variables
   	$options = [];

      if ($request->has('startDate')) {
         $options['startDate'] = $request->startDate;
      }

      if ($request->has('endDate')) {
         $options['endDate'] = $request->endDate;      
         $options['endDate'] = Carbon::parse($options['endDate'])->addDays(1)->addSeconds(-1)->toDateTimeString();
      }

      $options['organization_id'] = auth()->user()->organization_id;

      $data = Report::totalSalesMadeByEachAgent($options);
      $json = [
         'message' => 'Ventas total de cada agente',
         'data' => $data
      ];

      return response()->json($json);
   }

   public function totalDistanceTraveledOfAllAgents(Request $request)
   {
      $format = $request->has('format') ? $request->format : 'json';

      $options = [
         'organizationId' => auth()->user()->organization_id,
         'startDate' => $request->startDate,
         'endDate' => $request->endDate
      ];

      $items = Report::totalDistanceTraveledOfAllAgentsOfOrganization($options);

      switch ($format) {
         case 'xls':
            Excel::create('Recorrido de agentes', function ($excel) use ($items){
               $excel->sheet('', function ($sheet) use ($items) {

                  //Filling rows
                  $sheet->row(1, ['Nombre Agente', 'Recorrido en Km.']);
                  foreach ($items as $key => $item) {
                     $sheet->row($key + 2 , [$item['agentFullName'], $item['distance']]);
                  }
               });

            })->download();
         default:
               $json = [
                  'message' => 'Se obtuvo exitosamente la distancia recorrida de cada agente',
                  'data' => $items
               ];

               return response()->json($json);
            break;
      }
      
   }
}
