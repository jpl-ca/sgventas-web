<?php

namespace IrisGPS\Http\Controllers\Api;

use Illuminate\Http\Request;

use IrisGPS\Agent;
use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('auth:web');
    }

    public function index()
    {
        $agents = auth()->user()->organization->agents()->get();
        return response()->json($agents);
    }
}
