<?php

namespace IrisGPS\Http\Controllers\Api\Trackable\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\TasksVisitPoint;

use Carbon\Carbon;

class TaskVisitPointController extends Controller
{
	use GetUser;

    public function today()
    {
    	$organization = $this->getUser()->organization;
    	$items = $organization->tasksVisitPoints()
            ->where('tasks.taskable_type', get_class($this->getUser()))
            ->where('tasks.taskable_id', $this->getUser()->id)
            ->whereDate('tasks_visit_points.datetime', '=', Carbon::today()->toDateString())
            ->with('productOrder.productOrderItems')->get();
    	$json = [
    		'message' => 'Task visit point obtenido exitosamente',
    		'data' => $items
     	];

    	return response()->json($json);
    }
}
