<?php

namespace IrisGPS\Http\Controllers\Api\Trackable\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Controllers\BaseApiController;

use IrisGPS\Product;

class ProductController extends Controller
{
	public function __construct()
    {
   	 $this->classReference = Product::class;
    }

    use BaseApiController {
    	index as traintIndex;
    }

   	public function index(Request $request)
   	{
      $filters = $request->filters ? $request->filters : null;
      $organizationId = auth('api')->user()->trackable->organization_id;

      if (isset($filters) && is_array($filters)) {
         $filters[] = [
            'field' => 'organization_id',
            'value' => $organizationId
         ];

      } else {
         $filters[] = [
            'field' => 'organization_id',
            'value' => $organizationId
         ];
      }

      $request->merge(['filters' => $filters]);

      return $this->traintIndex($request);
   	}
}
