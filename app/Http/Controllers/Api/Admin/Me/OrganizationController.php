<?php

namespace IrisGPS\Http\Controllers\Api\Admin\Me;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Organization;
use IrisGPS\TasksVisitPoint;

use Maatwebsite\Excel\Facades\Excel;
use IrisGPS\Http\Controllers\BaseApiController;

class OrganizationController extends Controller
{
    use BaseApiController;

    public function __construct()
    {
        $this->classReference = Organization::class;
    }

    //use BaseApiController;
    public function reportOfaccountsCreated(Request $request)
    {
    	$format = $request->has('format') ? $request->format : 'json';
    	$items = Organization::itemsCreatedGroupedByDay([
    		'startDate' => $request->startDate,
    		'endDate' => $request->endDate
    	]);

    	// Making response
        switch ($format) {
            case 'xls':

            	Excel::create('Organizaciones creadas agrupadas por día', function($excel) use ($items) {
                    $excel->sheet('', function($sheet) use ($items) {
                        $sheet->row(1,
                        	[
	                            'Fecha',
	                            'Total'
                            ]);

                        $i = 2;

                        foreach ($items as $item) {
                            $sheet->row(
                            	$i,
                            	[
	                                $item->date,
	                                $item->total
                            	]
                            );

                            $i++;
                        }
                    });

                })->download();

                break;
            case 'json':
            default:
            	$json = [];
            	$json['data'] = $items;
            	return $json;
                break;
        }
    }

    public function reportOfAgents(Request $request)
    {
        $format = $request->has('format') ? $request->format : 'json';

        $items = Organization::listOfAgentsCreated([
            'startDate' => $request->startDate,
            'endDate' => $request->endDate
        ]);

        // Making response
        switch ($format) {
            case 'xls':

                Excel::create('Agentes VS Organizaciones', function($excel) use ($items) {
                    $excel->sheet('', function($sheet) use ($items) {
                        $sheet->row(1,
                            [
                                'Organizacion',
                                'Total'
                            ]);

                        $i = 2;

                        foreach ($items as $item) {
                            $sheet->row(
                                $i,
                                [
                                    $item->name,
                                    $item->total
                                ]
                            );

                            $i++;
                        }
                    });

                })->download();

                break;
            case 'json':
            default:
                $json = [];
                $json['data'] = $items;
                return $json;
                break;
        }
    }

    public function reportOfVisits(Request $request, $organizationId = null)
    {
        $format = isset($request->format) ? $request->format : 'json';

        //Validating
        if ($organizationId) {
            $organization = Organization::findOrFail($organizationId);
        }

        $items = TasksVisitPoint::itemsGroupedByDay(
            [
                'startDate' => $request->startDate,
                'endDate' => $request->endDate,
                'organizationId' => $organizationId
            ]
        );

        switch ($format) {
            case 'xls':

                Excel::create('Puntos de visita', function($excel) use ($items) {
                    $excel->sheet('', function($sheet) use ($items) {
                        $sheet->row(1,
                            [
                                'Fecha',
                                'Programados',
                                'Terminados',
                                'Reprogramados',
                                'Cancelados'
                            ]);

                        $i = 2;

                        foreach ($items as $item) {
                            $sheet->row(
                                $i,
                                [
                                    $item->date,
                                    $item->scheduled,
                                    $item->done,
                                    $item->rescheduled,
                                    $item->cancelled
                                ]
                            );

                            $i++;
                        }
                    });

                })->download();

                break;
            case 'json':
                    
            default:
                $json = [];
                $json = [
                    'data' => $items
                ];

                return response()->json($json);     
                break;
        }
    }
}
