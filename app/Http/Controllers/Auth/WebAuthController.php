<?php

namespace IrisGPS\Http\Controllers\Auth;

use IrisGPS\Organization;

use IrisGPS\User;

use Validator;

use IrisGPS\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;

use MultiAuth\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;

use IrisGPS\ActivationService;

class WebAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $guard = 'web';

    protected $activationService;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
    protected $redirectAfterLogout = '/login';
    protected $authenticationView = 'web.auth.login';
    protected $registrationView = 'web.auth.register';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(ActivationService $activationService)
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        //$this->middleware('guest', ['only' => ['getLogin', 'getRegister']]);
        $this->activationService = $activationService;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'pais' => 'required',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'organization' => 'required|max:255|min:5|unique:organizations,name',
            'organization_id' => 'required|unique:organizations,id',
            'ruc' => 'unique:organizations,ruc|doc_number_of_organization',
            'category' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $insert = [
            'id' => $data['organization_id'],
            'name' => $data['organization'],
            'ruc' => $data['ruc'],
            'country_suffix' => $data['pais'],
            'phone_number' => $data['phone_number'],
            'category' => $data['category'],
        ];

        $organization = Organization::create($insert);

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'organization_id' => $organization->id
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = $this->create($request->all());

        $this->activationService->sendActivationMail($user);

        return redirect('/login')->with('successful-registration', "Te hemos envíado un código de activación a esta dirección <b>{$user->email}</b>. <br/>Por favor, revisa tu correo electrónico.");
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
            $this->activationService->sendActivationMail($user);
            auth()->logout();            
            return back()->with('warning-authentication', 'Ud. necesita confirmar su cuenta. Te hemos enviado un código de activación, por favor, revisar tu correo.');
        }
        return redirect()->intended($this->redirectPath());
    }

    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            auth()->login($user);
            return redirect($this->redirectPath());
        }
        abort(404);
    }

    public function showRegistrationForm()
    {
        $categories = $this->getCategories();

        
        return view($this->registrationView, ['categories' => $categories]);
        
    }

    private function getCategories()
    {
        return [
            '' => 'Seleccione',
            'Ganadero' => 'Ganadero',
            'Pesquero' => 'Pesquero',
            'Minero' => 'Minero',
            'Forestal' => 'Forestal',
            'Industrial' => 'Industrial',
            'Energético' => 'Energético',
            'Transportes' => 'Transportes',
            'Comunicaciones' => 'Comunicaciones',
            'Comercial' => 'Comercial',
            'Turístico' => 'Turístico',
            'Sanitario' => 'Sanitario',
            'Educativo' => 'Educativo',
            'Financiero' => 'Financiero',
            'Administración' => 'Administración',
            'Otros' => 'Otros'
        ];
    }
}
