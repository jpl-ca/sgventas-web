<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\IWebController;

class ProductController extends Controller
{
    public function index(Request $request)
    {
    	return view('web.products.index');
    }

    public function create(Request $request)
    {
    	return view('web.products.create');
    }

    public function edit(Request $request, $id)
    {
    	$data = [
            'id' => $id
        ];

        return view('web.products.edit', $data);
    }

    public function show(Request $request, $id)
    {
        $data = [
            'id' => $id
        ];

        return view('web.products.show', $data);
    }

    public function update(Request $request)
    {

    }

    public function destroy(Request $request)
    {

    }
}
