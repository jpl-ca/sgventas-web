<?php

namespace IrisGPS\Http\Controllers\Web;

use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Http\Requests;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function landing()
    {
        return view('landing');
    }
}
