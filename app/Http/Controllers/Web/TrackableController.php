<?php

namespace IrisGPS\Http\Controllers\Web;

use IrisGPS\Agent;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Http\Requests;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests\StoreAgentRequest;

use IrisGPS\Http\Requests\StoreVehicleRequest;

use IrisGPS\Http\Requests\UpdateOrDeleteAgentRequest;

use IrisGPS\Http\Requests\UpdateOrDeleteVehicleRequest;

use IrisGPS\Trackable;

use IrisGPS\Vehicle;

use IrisGPS\Organization;

use IrisGPS\TrackableType;

use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;

class TrackableController extends Controller
{

    public function __contruct()
    {
        $this->middleware('verify-privilege:view-trackable', ['only' => 'view']);
        $this->middleware('verify-privilege:create-trackable', ['only' => ['createAgent','createVehicle'] ]);
        $this->middleware('verify-privilege:store-trackable', ['only' => ['storeAgent','storeVehicle'] ]);
        $this->middleware('verify-privilege:edit-trackable', ['only' => ['editAgent','editVehicle'] ]);
        $this->middleware('verify-privilege:destroy-trackable', ['only' => ['destroyAgent','destroyVehicle'] ]);
        $this->middleware('verify-privilege:edit-trackable', ['only' => ['updateAgent','updateVehicle'] ]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $format = 'html')
    {
        //Setting value of variables
        $isAgentQuery =false;
        $isVehicleQuery =false;
        $typeOfTrackable = null;

        $searchTextOfAgent = $request->search_text ? $request->search_text : null;
        $searchTypeOfAgent = $request->search_type ? $request->search_type : null;

        $searchTextOfVehicle = $request->search_text ? $request->search_text : null;
        $searchTypeOfVehicle = $request->search_type ? $request->search_type : null;
        
        $searchTypesOfAgent = [
            'first_name' => 'Nombre',
            'last_name' => 'Apellido'
        ];

        $searchTypesOfVehicle = [
            'plate' => 'Placa',
            'brand' => 'Marca',
            'model' => 'Modelo'
        ];

        $organization_id = (auth('web')->check() ? auth('web')->user()->organization_id : null);
        $organization = Organization::find($organization_id);

        $agents = $organization->agents();
        $vehicles = $organization->vehicles();

        $isAgentQuery = ($searchTypesOfAgent && array_key_exists($request->search_type, $searchTypesOfAgent));
        $isVehicleQuery = ($searchTypesOfVehicle && array_key_exists($request->search_type, $searchTypesOfVehicle));

        if ($isAgentQuery) {
            $searchTextOfAgent = $request->search_text ? $request->search_text : null;
            $searchTypeOfAgent = $request->search_type ? $request->search_type : null;
        }

        if ($isVehicleQuery) {
            $searchTextOfVehicle = $request->search_text ? $request->search_text : null;
            $searchTypeOfVehicle = $request->search_type ? $request->search_type : null;
        }
        
        $typeOfTrackable = $isAgentQuery ? TrackableType::AGENT : TrackableType::VEHICLE;

        //Building query
        if ($isAgentQuery) {
            $agents->where($searchTypeOfAgent, 'LIKE', "%{$searchTextOfAgent}%");
        }

        if ($isVehicleQuery) {
            $vehicles->where($searchTypeOfVehicle, 'LIKE', "%{$searchTextOfVehicle}%");
        }

        //Building paginator
        $agents = $agents->paginate(20);
        $vehicles = $vehicles->paginate(20);

        //Building response
        switch ($format) {
            case 'xls':
                Excel::create('Rastreables', function($excel) use ($agents, $vehicles, $typeOfTrackable) {

                switch ($typeOfTrackable) {

                    case TrackableType::AGENT:
                            $excel->sheet('Agentes', function($sheet) use ($agents) {
                                
                                $sheet->row(1, [
                                    'id',
                                    'Nombres',
                                    'Apellidos',
                                    'Registrado'
                                    ]);

                                $i = 2;

                                foreach ($agents as $key => $value) {

                                    $sheet->row($i, [
                                        $value->id,
                                        $value->first_name,
                                        $value->last_name,
                                        $value->created_at
                                        ]);
                                    $i++;
                                }
                            });
                        break;
                    
                    case TrackableType::VEHICLE:
                        $excel->sheet('Vehículos', function($sheet) use ($vehicles) {
                            
                            $sheet->row(1, [
                                'id',
                                'Placa',
                                'Marca',
                                'Modelo',
                                'Color', 
                                'Año de fabricación', 'Registrado'
                                ]);

                            $i = 2;

                            foreach ($vehicles as $key => $value) {
                                $sheet->row($i, [
                                    $value->id,
                                    $value->plate,
                                    $value->brand,
                                    $value->model,
                                    $value->color,
                                    $value->manufacture_year,
                                    $value->created_at
                                    ]);
                                $i++;
                            }
                        });

                        break;
                }
                    

                    
                })->download();

            default:
                return view('web.trackables.index')->with(compact(
                        'agents',
                        'vehicles',
                        'searchTextOfAgent',
                        'searchTypeOfAgent',
                        'searchTypesOfAgent',
                        'searchTextOfVehicle',
                        'searchTypeOfVehicle',
                        'searchTypesOfVehicle'
                    ));
        }
        
    }

    public function createAgent()
    {
        return view('web.trackables.agents.create');
    }

    public function storeAgent(StoreAgentRequest $request)
    {
        $agent = new Agent;
        $agent->first_name = $request->first_name;
        $agent->last_name = $request->last_name;
        $agent->authentication_code = $request->authentication_code;
        $agent->password = bcrypt($request->password);
        $agent->save();

        return response()
            ->redirectToAction('Web\TrackableController@index')
            ->withSuccess('Agente creado exitosamente.');
    }

    public function viewAgent(Agent $agent)
    {
        $this->validateTrackableProperty($agent);
        $tasks = $agent->tasks;
        return view('web.trackables.agents.view')->with(compact('agent', 'tasks'));
    }

    public function editAgent(Agent $agent)
    {
        $this->validateTrackableProperty($agent);
        return view('web.trackables.agents.edit')->with(compact('agent'));
    }

    public function updateAgent(UpdateOrDeleteAgentRequest $request, Agent $agent)
    {
        $agent->first_name = $request->first_name;
        $agent->last_name = $request->last_name;
        $agent->authentication_code = $request->authentication_code;
        $agent->password = empty($request->password) ? $agent->password : bcrypt($request->password);
        $agent->save();

        return response()
            ->redirectToAction('Web\TrackableController@index')
            ->withSuccess('Agente actualizado exitosamente.');
    }

    public function createVehicle()
    {
        return view('web.trackables.vehicles.create');
    }

    public function storeVehicle(StoreVehicleRequest $request)
    {
        $vehicle = new Vehicle;
        $vehicle->plate = $request->plate;
        $vehicle->brand = $request->brand;
        $vehicle->model = $request->model;
        $vehicle->color = $request->color;
        $vehicle->manufacture_year = $request->manufacture_year;
        $vehicle->gas_consumption_rate = $request->gas_consumption_rate;
        $vehicle->password = bcrypt($request->password);
        $vehicle->save();

        return response()
            ->redirectToAction('Web\TrackableController@index')
            ->withSuccess('Vehículo creado exitosamente.');
    }

    public function viewVehicle(Vehicle $vehicle)
    {
        $this->validateTrackableProperty($vehicle);
        $tasks = $vehicle->tasks;
        return view('web.trackables.vehicles.view')->with(compact('vehicle', 'tasks'));
    }

    public function editVehicle(Vehicle $vehicle)
    {
        $this->validateTrackableProperty($vehicle);
        return view('web.trackables.vehicles.edit')->with(compact('vehicle'));
    }

    public function updateVehicle(UpdateOrDeleteVehicleRequest $request, Vehicle $vehicle)
    {
        $vehicle->plate = $request->plate;
        $vehicle->plate = $request->plate;
        $vehicle->brand = $request->brand;
        $vehicle->model = $request->model;
        $vehicle->color = $request->color;
        $vehicle->manufacture_year = $request->manufacture_year;
        $vehicle->gas_consumption_rate = $request->gas_consumption_rate;
        $vehicle->password = empty($request->password) ? $vehicle->password : bcrypt($request->password);
        $vehicle->save();

        return response()
            ->redirectToAction('Web\TrackableController@index')
            ->withSuccess('Vehículo actualizado exitosamente.');
    }

    public function destroy(Trackable $trackable)
    {
        $this->validateTrackableProperty($trackableModel = $trackable->trackable);
        $message = ucfirst($trackable->type()) . ' eliminado exitosamente.';
        $trackableModel->delete();
        return back()->withSuccess($message);
    }

    public function geolocationHistoryAgent(Request $request, $agentId)
    {
        $agent = Agent::find($agentId);

        if (isset($request->startDate)) {
            $startDate = Carbon::parse($request->startDate);
        } else {
            if (!isset($request->endDate)) {
                $startDate = Carbon::now()->startOfDay();
            }
        }

        if (isset($request->endDate)) {
            $endDate = Carbon::parse($request->endDate);
        } else {
            $endDate = $startDate->copy()->endOfDay();
        }

        if (!isset($startDate)) {
            $startDate = $endDate->copy()->startOfDay();
        }

        $data = [
            'startDate' => $startDate->toDateTimeString(),
            'endDate' => $endDate->toDateTimeString(),
            'agent' => $agent,
            'debug' => $request->exists('debug')
        ];

        return view('web.trackables.agents.geolocation-history', $data);
    }

    protected function validateTrackableProperty($model)
    {
        if ($model->organization_id !== auth()->user()->organization_id) {
            abort(404);
        }
    }
}
