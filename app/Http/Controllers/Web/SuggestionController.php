<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;

class SuggestionController extends Controller
{
	public function create()
	{
		return view('web.suggestions.create');
	}
}
