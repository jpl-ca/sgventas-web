<?php

namespace IrisGPS\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;

use IrisGPS\Http\Controllers\Controller;

use IrisGPS\Organization;

use IrisGPS\User;

use IrisGPS\Privilege;

use IrisGPS\OrganizationPayment;

use Log;

class OrganizationPaymentController extends Controller
{
    public function index(Request $request, $organization_id, $format = 'html')
    {
        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;

        $searchTypes = array(
            'amount' => 'Monto',
            'additional_information' => 'Información adicional',
        );

        $organization = Organization::findOrFail($organization_id);
        //Building Query
        $items = OrganizationPayment::query()->orderBy('created_at', 'desc');

        //Searching items
        if ($searchType && $searchText) {
            $items = $items->where(
                $searchType,
                'LIKE',
                '%' . $searchText . '%'
            );
        }

        //Building paginator
        $items = $items->paginate();

        //data 
        $data = compact(
            'organization',
            'items',
            'searchText',
            'searchType',
            'searchTypes'
        );

        return view('web.admin.organizations.payments.index', $data);
    }

    public function create(Request $request, $organization_id)
    {
        $organization = Organization::find($organization_id);

        $data = compact(
            'organization'
        );

        return view('web.admin.organizations.payments.create', $data);
    }

    public function edit(Request $request, $organization_id, $paymentId)
    {
        $item = OrganizationPayment::findOrFail($paymentId);
        $data = compact('item');
        return view('web.admin.organizations.payments.edit', $data);
    }

    public function view(Request $request,$organization_id, $paymentId)
    {
        $item = OrganizationPayment::findOrFail($paymentId);
        $organization = Organization::findOrFail($organization_id);
        $data = compact(
            'item',
            'organization'
        );
        return view('web.admin.organizations.payments.view', $data);
    }

    public function update(Request $request, $organization_id, $paymentId)
    {
        $item = OrganizationPayment::findOrFail($paymentId);
        $item->fill($request->all());
        $item->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            
            $filename = $item->id;
            
            $directoryPath = 'app/organization-payments/';

            $item->image_path = $directoryPath . $filename;

            $directoryPath = storage_path($directoryPath);

            Log::info("Moving file to " . $directoryPath);

            $request->file('image')->move($directoryPath, $filename);

            
            $item->save();
        }

        return response()->redirectTo(
                action(
                    'Web\Admin\OrganizationPaymentController@index',
                    [
                        'organization_id' => $item->organization_id,
                        'format' => 'html'
                    ]
                )
            )
            ->withSucess('Actualizado exitosamente');
    }

    public function destroy(Request $request, $organization_id, $paymentId)
    {
        $item = OrganizationPayment::findOrFail($paymentId);
        $item->delete();

        return response()
            ->redirectTo(action('Web\Admin\OrganizationPaymentController@index', ['organization_id' => $organization_id, 'format' => 'html']))
            ->withSucess('Eliminado exitosamente');

    }

    public function store(Request $request, $organization_id)
    {

        $newItem = new OrganizationPayment;
        $newItem->fill($request->all());
        $newItem->organization_id = $organization_id;
        $newItem->save();

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $filename = $newItem->id;
            
            $directoryPath = 'app/organization-payments/';

            $newItem->image_path = $directoryPath . $filename;

            $directoryPath = storage_path($directoryPath);
            
            Log::info("Moving file to " . $directoryPath);

            $request->file('image')->move($directoryPath, $filename);

            $newItem->save();
        }

        $organization = Organization::find($organization_id);

        $organization->payments()->save($newItem);

        return response()
            ->redirectTo(action('Web\Admin\OrganizationPaymentController@index', ['organization_id' => $organization_id, 'format' => 'html'] ))
            ->withSucess('Pago registrado exitosamente');
    }

    public function viewImage(Request $request, $organization_id, $paymentId)
    {
        $item = OrganizationPayment::findOrFail($paymentId);
        $path = storage_path($item->image_path);
        return response()->download($path);
    }
}
