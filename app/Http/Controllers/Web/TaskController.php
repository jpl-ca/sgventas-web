<?php

namespace IrisGPS\Http\Controllers\Web;

use Illuminate\Http\Request;

use IrisGPS\Http\Requests;
use IrisGPS\Http\Controllers\Controller;
use IrisGPS\Task;

use Maatwebsite\Excel\Facades\Excel;

class TaskController extends Controller
{
    public function __construct()
    {
        if(!auth('admin')->check()) {
            $this->middleware('verify-organization');
            $this->middleware('verify-privilege:manage-tasks', ['only' => 'calendar']);
        }
    }

    public function index(Request $request, $format = 'html')
    {

        $tasks = Task::query();
        $searchText = $request->search_text ? $request->search_text : null;
        $searchType = $request->search_type ? $request->search_type : null;
        $organization_id = (auth('web')->check() ? auth('web')->user()->organization_id : null);

        $searchTypes = array(
            'description' => 'Descripción',
            'trackable-agent' => 'Agente > nombre',
            'trackable-vehicle' => 'Vehículo > placa'
        );

        //Building query
        if ($searchType && $searchText) {
            if (in_array($searchType, ['description'])) {
                $tasks = $tasks->where(
                    $searchType,
                    'LIKE',
                    '%' . $searchText . '%'
                );    
            }

            if ($searchType == 'trackable-agent') {
                $tasks->join('trackables', 'tasks.taskable_id', '=', 'trackables.id');
                $tasks->join('agents', 'agents.id', '=', 'trackables.trackable_id');
                $tasks->where('trackables.trackable_type', 'IrisGPS\Agent');
                $tasks->where('agents.first_name', 'LIKE', "%{$searchText}%");
            }

            if ($searchType == 'trackable-vehicle') {
                $tasks->join('trackables', 'tasks.taskable_id', '=', 'trackables.trackable_id');
                $tasks->join('vehicles', 'vehicles.id', '=', 'trackables.trackable_id');
                $tasks->where('trackables.trackable_type', 'IrisGPS\Vehicle');
                $tasks->where('vehicles.plate', 'LIKE', "%{$searchText}%");
            }
        }

        $tasks->where('tasks.organization_id', $organization_id);

        //Building paginator
        $tasks = $tasks->paginate(20);

        switch ($format) {
            case 'xls':
                Excel::create('Rutas', function($excel) use ($tasks) {
                    $excel->sheet('Rutas', function($sheet) use ($tasks) {
                        //$sheet->row(1, ['id','Descripción', 'Inicio', 'Fin']);
                        //$i = 2;
                        $nextIndex = 0;
                        foreach ($tasks as $key => $value) {

                            //Ruta name
                            $nextIndex++;
                            $sheet->row($nextIndex, ['RUTA: ' . $value->id]);

                            //Description
                            $nextIndex++;
                            $sheet->row($nextIndex, ['Descripción', $value->description]);

                            // Assigned to
                            $nextIndex++;
                            if ($value->taskable) {
                                $fullName = $value->taskable->trackable->full_name;
                            } else {
                                $fullName = 'Sin definir';
                            }

                            $sheet->row($nextIndex, ['Asignado a', $fullName]);

                            // Visits
                            $nextIndex++;
                            $sheet->row($nextIndex, ['Puntos a visitar']);
                            
                            $nextIndex++;
                            $sheet->row($nextIndex, ['', 'Estado', 'Dirección', 'Hora', 'Tareas']);
                            foreach ($value->visitPoints as $visit) {
                                
                                $nextIndex++;

                                $checklist = [];
                                // Building string of checklist 
                                foreach ($visit->checklist as $item) {
                                    $checklist[] = $item->description;
                                }
                                $checklist = join(',', $checklist);

                                // Set into cells
                                $sheet->row($nextIndex, ['',
                                    $visit->state->name_to_spanish,
                                    $visit->address,
                                    $visit->time,
                                    $checklist
                                ]);
                            }

                            $nextIndex++;
                            $sheet->row($nextIndex, ['']);
                        }
                    });

                })->download();

                break;

            case 'html':
                return view('web.tasks.index',
                    compact(
                            'tasks',
                            'searchText',
                            'searchType',
                            'searchTypes'
                        ));
                break;
        }
    }

    public function create()
    {
        return view('web.tasks.create');

    }

    public function store()
    {

    }

    public function view(Task $task)
    {
        return view('web.tasks.view')->with(compact('task'));
    }

    public function edit(Task $task)
    {
        return view('web.tasks.edit')->with(compact('task'));
    }

    public function update()
    {

    }

    public function destroy(Task $task)
    {
        $task->delete();
        return redirect()->action('Web\TaskController@index')->withSuccess('¡ Eliminado exitosamente !');
    }

    public function calendar(Request $request)
    {
       return view('web.tasks.calendar');
    }
}
