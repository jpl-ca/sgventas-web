<?php

namespace IrisGPS\Http\Requests;

use IrisGPS\User;
use IrisGPS\Http\Requests\Request;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|max:255|email|unique:users,email',
            'password' => 'required|max:255|min:6',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->redirectToAction('WebDashboardController@dashboard');
    }
}
