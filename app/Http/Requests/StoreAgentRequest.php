<?php

namespace IrisGPS\Http\Requests;

use IrisGPS\Agent;
use IrisGPS\Http\Requests\Request;

class StoreAgentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $organizationId = auth()->user()->organization_id;
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'authentication_code' => "required|alpha_dash|min:4|max:255|unique:agents,authentication_code,NULL,id,organization_id,{$organizationId}",
            'password' => 'required|alpha_num|max:255|min:4',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->redirectToAction('WebDashboardController@dashboard');
    }
}
