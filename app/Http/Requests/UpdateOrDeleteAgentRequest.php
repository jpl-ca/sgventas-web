<?php

namespace IrisGPS\Http\Requests;

use IrisGPS\Agent;
use IrisGPS\Http\Requests\Request;

class UpdateOrDeleteAgentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $agentModel = $this->route('agentModel');

        return Agent::where('id', $agentModel->id)
            ->where('organization_id', auth()->user()->organization_id)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'authentication_code' => 'required|alpha_dash|min:4|max:255|unique:agents,authentication_code,' . $this->authentication_code . ',authentication_code',
            'password' => 'alpha_num|max:255|min:4',
        ];
    }

    public function forbiddenResponse()
    {
        return response()->redirectToAction('WebDashboardController@dashboard');
    }
}
