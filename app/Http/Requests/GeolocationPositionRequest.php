<?php

namespace IrisGPS\Http\Requests;

use IrisGPS\Http\Requests\Request;

class GeolocationPositionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lat' => 'required|numeric|min:-90.999999999999999|max:90.999999999999999',
            'lng' => 'required|numeric|min:-180.999999999999999|max:180.999999999999999',
            // 'task_id' => 'can_be_done_by:' . $trackableModel->id . ',' . get_class($trackableModel),
            'positions' => 'array',
            'positions.*.lat' => 'required|numeric|min:-90.999999999999999|max:90.999999999999999',
            'positions.*.lng' => 'required|numeric|min:-180.999999999999999|max:180.999999999999999',
        ];
    }
}
