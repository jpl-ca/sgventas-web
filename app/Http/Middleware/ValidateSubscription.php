<?php

namespace IrisGPS\Http\Middleware;

use Closure;

class ValidateSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (auth($guard)->check()) {
            if ($request->ajax() || $request->wantsJson()) {
                $organization = auth($guard)->user()->trackable->organization;
                if ($organization->isCurrentPlanExpired()) {
                    return response('Your subscription has been expired.', 403);
                }
            } else {
                $organization = auth('web')->user()->organization;
                if ($organization->isCurrentPlanExpired() && $organization->isInGracePeriod()) {
                    $request->session()->flash('warning',
                        '<strong>Your subscription has been expired. Now your organization has '. $organization->subscriptionRemainingGraceDays() .' remaining day(s) to renew their subscription or all their tracking data will be lost.</strong>');
                }
            }
        }
        return $next($request);
    }
}
