<?php

namespace IrisGPS;

use Illuminate\Mail\Mailer;

use Illuminate\Mail\Message;

use Mail;

class ActivationService
{

    protected $mailer;

    protected $activationRepo;

    protected $resendAfter = 24;

    public function __construct(Mailer $mailer, ActivationRepository $activationRepo)
    {
        $this->mailer = $mailer;
        $this->activationRepo = $activationRepo;
    }

    public function sendActivationMail($user)
    {

        if ($user->activated || !$this->shouldSend($user)) {
            return;
        }

        $token = $this->activationRepo->createActivation($user);

        $link = route('user.activate', $token);

        $logoUrl = 'http://sgventas.sgtel.pe/assets/irisgps/img-web/logo.png';

        $emailData = ['user' => $user,
            'link' => $link,
            'logoUrl' => $logoUrl
        ];

        Mail::send('emails.email-confirmation', $emailData, function ($m) use ($user) {
            $m->from('postmaster@sgtel.pe', 'Soporte SGTEL');
            $m->to($user->email, $user->name)->subject('Confirmación de correo electrónico | SGTEL');
        });


    }

    public function activateUser($token)
    {
        $activation = $this->activationRepo->getActivationByToken($token);

        if ($activation === null) {
            return null;
        }

        $user = User::find($activation->user_id);

        $user->activated = true;

        $user->save();

        $this->activationRepo->deleteActivation($token);

        return $user;

    }

    private function shouldSend($user)
    {
        $activation = $this->activationRepo->getActivation($user);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

}