<?php

namespace IrisGPS;

use Illuminate\Database\Eloquent\Model;

use Log;

use Carbon\Carbon;

use DB;

class TasksVisitPoint extends Model
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['checklist_items', 'checklist_done_items', 'status', 'time'];

    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    public function state()
    {
        return $this->belongsTo(VisitState::class, 'visit_state_id', 'id');
    }

    public function checklist()
    {
        return $this->hasMany(ChecklistItem::class, 'tasks_visit_point_id', 'id');
    }

    public function getStatusAttribute()
    {
        if ($this->state && $this->state->name) {
            return strtolower(str_slug($this->state->name, '_'));     
        }
        
    }

    public function getChecklistItemsAttribute()
    {
        return $this->checklist->count();
    }

    public function getChecklistDoneItemsAttribute()
    {
        return $this->checklist()->where('checked', true)->count();
    }

    public function getStateLabel() {
        switch ($this->visit_state_id) {
            case VisitState::STATE_SCHEDULED:
                return 'scheduled';
            case VisitState::STATE_DONE:
                return 'done';
            case VisitState::STATE_RESCHEDULING_REQUEST:
                return 'rescheduling-request';
            case VisitState::STATE_CANCELLED:
                return 'cancelled';
        }
    }

    public function getIcon()
    {
        switch($this->visit_state_id) {
            case VisitState::STATE_SCHEDULED:
                return 'markers/scheduled.png';
            case VisitState::STATE_DONE:
                return 'markers/done.png';
            case VisitState::STATE_RESCHEDULING_REQUEST:
                return 'markers/rescheduling_request.png';
        }
    }

    public function getInfoWindow()
    {
        return view('web.tasks.visit-points.partials.info-window')->with('visitPoint', $this)->render();
    }

    public function child()
    {
        return $this->belongsTo(TasksVisitPoint::class, 'visit_point_id_parent', 'id');
    }

    public function children()
    {
        $items = [];
        $limit = 100;
        $lastItem = $this;
        $i = 0;

        do {
            
            $lastItem = $lastItem->child ? $lastItem->child : null;

            if ($lastItem) {
                $lastItem->task;
                $items[] = $lastItem;
            }

            $i++;
        } while (isset($lastItem) && $i < $limit);

        return $items;
    }

    public function getTimeAttribute()
    {
        $output = '';
        if(isset($this->datetime)) {
            $output = Carbon::parse($this->datetime)->format("h:i A");
        }

        return $output;
    }

    public static function itemsGroupedByDay($options = [])
    {
        // Setting initial values
        $startDate = isset($options['startDate']) ? $options['startDate'] : null; 
        $endDate = isset($options['endDate']) ? $options['endDate'] : null;

        if (isset($endDate)) {
            $endDate = Carbon::parse($options['endDate'])->addDays(1)->addSeconds(-1)->toDateTimeString();
        }

        $trackableId = null;
        $sqlParams = [];
        $agentId = null;
        $vehicleId = null;
        $organizationId = null;

        if (
                isset($options['trackableType']) &&
                isset($options['trackableId']) &&
                $options['trackableType'] == 'agent'
            ) {
            $agentId = $options['trackableId'];
        }

        if (
                isset($options['trackableType']) &&
                isset($options['trackableId']) &&
                $options['trackableType'] == 'vehicle'
            ) {
            $vehicleId = $options['trackableId'];
        }

        if (isset($options['organizationId'])) {
            $organizationId = $options['organizationId'];
        }

        $query = '
            select 
                date(tasks_visit_points.datetime) date,
                sum(if(tasks_visit_points.visit_state_id=1,1,0)) as scheduled,
                sum(if(tasks_visit_points.visit_state_id=2,1,0)) as done,
                sum(if(tasks_visit_points.visit_state_id=3,1,0)) as rescheduled,
                sum(if(tasks_visit_points.visit_state_id=4,1,0)) as cancelled
            from
                tasks_visit_points
            ';

        if ($agentId || $vehicleId || $organizationId) {
            $query .= "
            INNER JOIN tasks
                ON(tasks.id = tasks_visit_points.task_id)
            ";

        }

        // Adding Where Statements
        $query .= "WHERE 1=1";

        if ($agentId) {
            $query .= '
             AND tasks.taskable_type = \'IrisGPS\\\Agent\'
            ';

            $query .= "
             AND tasks.taskable_id = :trackableId
            ";
            $trackableId = $agentId;
        }

        if ($vehicleId) {
            $query .= "
             AND tasks.taskable_type = 'IrisGPS\\\Vehicle'
            ";

            $query .= "
             AND tasks.taskable_id = :trackableId
            ";
            $trackableId = $vehicleId;
        }

        if ($organizationId) {
            $query .= "
             AND tasks.organization_id = :organizationId
            ";
        }

        if ($startDate) {
            $query .= " AND tasks_visit_points.datetime >= :startDate";
        }

        if ($endDate) {
            $query .= " AND tasks_visit_points.datetime <= :endDate";
        }

        // Adding Group Statements
        $query .= '
        group by
                date(tasks_visit_points.datetime)

        order by
                date(tasks_visit_points.datetime) asc';

        if ($startDate) $sqlParams['startDate'] = $startDate;
        if ($endDate) $sqlParams['endDate'] = $endDate;
        if ($trackableId) $sqlParams['trackableId'] = $trackableId;
        if ($organizationId) $sqlParams['organizationId'] = $organizationId; 

        $rows = DB::select($query, $sqlParams);

        return $rows;
    }

    public function productOrder()
    {
        return $this->hasOne(ProductOrder::class);
    }

    public function getHexadecimalColorAttribute()
    {
        $color = null;
        $visitStateId = (int) $this->visit_state_id;
        switch ($visitStateId) {
            case VisitState::STATE_SCHEDULED:
                $color = "8E24AA";
                break;

            case VisitState::STATE_RESCHEDULING_REQUEST:
                $color = "00BCD4";
                break;

            case VisitState::STATE_DONE:
                $color = "3F51B5";
                break;
            default:
                $color = "8E24AA";
                break;
        }
        return $color;
    }
}
