<?php

namespace IrisGPS\Observers;

use IrisGPS\TasksVisitPointHistory;

use IrisGPS\TasksVisitPoint;

use IrisGPS\VisitState;

use Log;

use Carbon\Carbon;

use IrisGPS\Task;

use IrisGPS\Agent;

use IrisGPS\Vehicle;

use IrisGPS\ProductOrder;

class ProductOrderObserver
{
	public function saving($model)
	{
		$this->settingTrackable($model);
	}

	public function settingTrackable($model)
	{
		$trackable = $model->visitPoint->task->taskable;
		//dd($trackable);
		$model->trackable_type = get_class($trackable);
		$model->trackable_id = $trackable->id;

		if (get_class($trackable) == Agent::class) {
			$model->trackable_fullname = $trackable->first_name . ' ' .$trackable->last_name;
		}

		if (get_class($trackable) == Vehicle::class) {
			$model->trackable_fullname = $trackable->plate;
		}
	}

	public function saved($model)
	{
		if ($model && $model->tasks_visit_point_id) {
			$taskVisitPoint = TasksVisitPoint::find($model->tasks_visit_point_id);
			$taskVisitPoint->visit_state_id = VisitState::STATE_DONE;
			$taskVisitPoint->save();
		}

		if ($model->isDirty('status') && $model->status == 'confirmed') {
			$productOrder = ProductOrder::find($model->id);
			if ($productOrder->productOrderItems()->count() > 0) {
				foreach($productOrder->productOrderItems as $orderItem) {
					if ($orderItem->product) {
						$tmpProduct = $orderItem->product;
						$tmpProduct->stock -=  $orderItem->quantity;
						$tmpProduct->save();
					}
				}
			}
		}

		if ($model->isDirty('status') && $model->status == 'canceled') {
			$productOrder = ProductOrder::find($model->id);
			if ($productOrder->productOrderItems()->count() > 0) {
				foreach($productOrder->productOrderItems as $orderItem) {
					if ($orderItem->product) {
						$tmpProduct = $orderItem->product;
						$tmpProduct->stock +=  $orderItem->quantity;
						$tmpProduct->save();
					}
				}
			}
		}
	}
}