<?php

namespace IrisGPS\Observers;

use IrisGPS\TasksVisitPointHistory;

use IrisGPS\TasksVisitPoint;

use IrisGPS\VisitState;

use Log;

use Carbon\Carbon;

use IrisGPS\Product;

class ProductOrderItemObserver
{
	public function saving($model)
	{
		$this->settingModel($model);
	}

	private function settingModel($model)
	{
		if (isset($model->product_id)) {

			$product = Product::find($model->product_id);
			if ($product) {
				$model->name = $product->name;
				$model->description = $product->description;
				$model->cache_description = $product->description;
				$model->cache_category = $product->category;
				$model->cache_brand = $product->brand;
				$model->cache_model = $product->model;
			}
		}
	}
}