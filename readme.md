#SGTareas

##Introduction

**SGTareas** is a tracking, task planning and route management platform, designed to track people and vehicles, assign tasks to them, set up destinations to visit and display tracking and activity information in a dynamic map.

**SGTareas** is an affordable solution that uses Android devices for tracking and task and activity management allowing the company to manage results and information at real time.

##This awesome project considerations

- Language 
	- Any contributor or team member ***MUST*** use ***English*** as language to create any kind of material related with ***development***, this includes: 
		- Code documentation
		- Database diagrams
		- Entity-Relationship diagrams
		- File naming
		- Inside code naming including variables and constants
		- Routing and group routing for actions and web services
	- Any contributor or team member ***MUST*** use ***Spanish*** as language to create any kind of material related with the ***business logic***, this includes:
		- Business requirements (functional, non-functional, system and domain requirements)
		- Business Use Case diagrams
		- Systems Use Case diagrams
		- Business plan
		- Marking plan
		- Pricing plan and features descriptions
- Repository and Branch Manage
	- Any contributor or team member ***MUST*** track all development progress using project's official repositories.
	- Any contributor or team member ***MUST*** create a new branch in case of:
		- Needs to implement a new feature to the current ***dev*** branch, that should be deleted and merged to ***dev*** when all the work in the ***"new-feature branch"*** is finished.
		- Needs to modify the current and established business logic, for example, add one field to log-in process. When this happens this should be considered as a ***New Distribution*** that ***NEVER*** should be merged to the current ***dev*** branch, and ***MUST*** be tagged as accordingly. Additionally could exist ***dev distribution*** branches that  ***MUST*** be named as ***dev-[distro-name]***.
		- Needs to implement a bug fix  to the current ***dev*** branch, that should be deleted and merged to ***dev*** when all the work in the ***"bux-fix branch"*** is finished.

##Installation

 1. Clone repository from Bitbucket
 2. Copy `.env.example` file. and remane in `.env` with the command: `cp .env.example .env`
 3. Install project dependencies using composer, run: `composer install`
 4. Be sure to at **scheduler** to the user's **cronjob**:
 `crontab -e`
 and at this line
 ` * * * * * php /path/to/sgtareas schedule:run >> /dev/null 2>&1`


##Modules

###CMS

###Android Application

View [SGTareas Android Application repository](https://bitbucket.org/jmtech/sgtareas-app)