<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisitStateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $date = Carbon\Carbon::create()->toDateTimeString();

        $programmedState = ['name' => 'Scheduled', 'created_at' => $date, 'updated_at' => $date];

        $doneState = ['name' => 'Done', 'created_at' => $date, 'updated_at' => $date];

        $reschedulingState = ['name' => 'Rescheduling Request', 'created_at' => $date, 'updated_at' => $date];

        $cancelledState = ['name' => 'Cancelled', 'created_at' => $date, 'updated_at' => $date];

        $db = DB::table('visit_states')->insert($programmedState);

        $db = DB::table('visit_states')->insert($doneState);

        $db = DB::table('visit_states')->insert($reschedulingState);

        $db = DB::table('visit_states')->insert($cancelledState);

        Model::reguard();
    }
}
