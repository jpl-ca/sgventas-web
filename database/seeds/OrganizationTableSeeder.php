<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use IrisGPS\Organization;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Organization::create([
            'id' => 'pepe-sac',
            'name' => 'pepe-sac'
        ]);

        Organization::create([
            'id' => 'root',
            'name' => 'root'
        ]);
    }
}
