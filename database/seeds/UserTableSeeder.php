<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use IrisGPS\User;
use IrisGPS\Organization;
use IrisGPS\Privilege;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Making root user
        $rootUser = User::create([
            'name' => 'root',
            'email' => 'root@yopmail.com',
            'password' => bcrypt('password'),
            'organization_id' => 'root'
        ]);

        //Making first test user
        $user = User::create([
            'name' => 'pepe-sac',
            'email' => 'pepe-sac@yopmail.com',
            'password' => bcrypt('password'),
            'organization_id' => Organization::where('id','pepe-sac')->first()->id
            ]
            );

        $privileges = Privilege::all();
        $userPrivileges = [];
        $date = Carbon::create()->toDateTimeString();
        foreach ($privileges as $privilege) {
            array_push($userPrivileges, [
                'user_id' => $user->id,
                'privilege_id' => $privilege->id,
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
        $db = DB::table('user_privileges')->insert($userPrivileges);

    }
}
