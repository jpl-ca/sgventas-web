<?php

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

use IrisGPS\Privilege;

use IrisGPS\User;

class PrivilegeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        $date = Carbon\Carbon::create();

        $privileges = [
            ['id' => 'create-user', 'name' => 'Crear usuario', 'created_at' => $date->addSeconds(1), 'updated_at' => $date->addSeconds(1)],
            ['id' => 'store-user', 'name' => 'Registrar usuario', 'created_at' => $date->addSeconds(2), 'updated_at' => $date->addSeconds(2)],
            ['id' => 'view-user', 'name' => 'Ver usuario', 'created_at' => $date->addSeconds(3), 'updated_at' => $date->addSeconds(3)],
            ['id' => 'edit-user', 'name' => 'Editar usuario', 'created_at' => $date->addSeconds(4), 'updated_at' => $date->addSeconds(4)],
            ['id' => 'destroy-user', 'name' => 'Eliminar usuario', 'created_at' => $date->addSeconds(5), 'updated_at' => $date->addSeconds(5)],
            #Trackables - agents
            ['id' => 'create-trackable-agent', 'name' => 'Crear agente', 'created_at' => $date->addSeconds(6), 'updated_at' => $date->addSeconds(6)],
            ['id' => 'store-trackable-agent', 'name' => 'Registrar agente', 'created_at' => $date->addSeconds(7), 'updated_at' => $date->addSeconds(7)],
            ['id' => 'view-trackable-agent', 'name' => 'Ver agente', 'created_at' => $date->addSeconds(8), 'updated_at' => $date->addSeconds(8)],
            ['id' => 'edit-trackable-agent', 'name' => 'Editar agente', 'created_at' => $date->addSeconds(9), 'updated_at' => $date->addSeconds(9)],
            ['id' => 'destroy-trackable-agent', 'name' => 'Eliminar agente', 'created_at' => $date->addSeconds(10), 'updated_at' => $date->addSeconds(10)],
            #Markers
            ['id' => 'create-marker', 'name' => 'Crear cliente', 'created_at' => $date->addSeconds(11), 'updated_at' => $date->addSeconds(11)],
            ['id' => 'store-marker', 'name' => 'Registra cliente', 'created_at' => $date->addSeconds(12), 'updated_at' => $date->addSeconds(12)],
            ['id' => 'view-marker', 'name' => 'Ver cliente', 'created_at' => $date->addSeconds(13), 'updated_at' => $date->addSeconds(13)],
            ['id' => 'edit-marker', 'name' => 'Editar cliente', 'created_at' => $date->addSeconds(14), 'updated_at' => $date->addSeconds(14)],
            ['id' => 'destroy-marker', 'name' => 'Eliminar cliente', 'created_at' => $date->addSeconds(15), 'updated_at' => $date->addSeconds(15)],
            #Tasks
            ['id' => 'manage-tasks', 'name' => 'Gestionar Programación', 'created_at' => $date->addSeconds(16), 'updated_at' => $date->addSeconds(16)],
            #Reports
            ['id' => 'manage-reports', 'name' => 'Gestionar Reportes', 'created_at' => $date->addSeconds(17), 'updated_at' => $date->addSeconds(17)],
            #Products
            ['id' => 'manage-products', 'name' => 'Gestionar Productos', 'created_at' => $date->addSeconds(18), 'updated_at' => $date->addSeconds(18)],
            #Product orders
            ['id' => 'manage-product-orders', 'name' => 'Gestionar Pedidos', 'created_at' => $date->addSeconds(18), 'updated_at' => $date->addSeconds(18)],
        ];

        DB::table('privileges')->delete();

        $db = DB::table('privileges')->insert($privileges);

        // Assign privileges to all users

        foreach (User::all() as $user) {
            $user->privileges()->delete();

            $date = Carbon\Carbon::create();
            foreach (Privilege::orderBy('created_at', 'asc')->get() as $privilege) {
                $db = DB::table('user_privileges')->insert([
                    'user_id' => $user->id,
                    'privilege_id' => $privilege->id,
                    'created_at' => $date,
                    'updated_at' => $date,
                ]);
            }
        }

        Model::reguard();
    }
}
