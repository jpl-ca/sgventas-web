<?php

use Illuminate\Database\Seeder;

use IrisGPS\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        	[
	        	'name' => 'Mouse inalámbrico',
	        	'price' => 20,
	        	'stock' => 123,
	        	'description' => 'Mouse inalambrico',
	        	'category' => 'periféricos',
	        	'brand' => 'Genius',
	        	'model' => 'YHG123345',
	        	'organization_id' => 'pepe-sac'
	        ],
	        [
	        	'name' => 'Mouse',
	        	'price' => 20,
	        	'stock' => 87,
	        	'description' => 'Mouse',
	        	'category' => 'periféricos',
	        	'brand' => 'LG',
	        	'model' => 'LG523341',
	        	'organization_id' => 'pepe-sac'
	        ],
        	[
	        	'name' => 'Computadora Micronics',
	        	'price' => 2150.00,
	        	'stock' => 80,
	        	'description' => 'Procesador ',
	        	'category' => 'computadoras',
	        	'brand' => 'Genius',
	        	'model' => 'YHG123345',
	        	'organization_id' => 'pepe-sac'
	        ],
	        [
	        	'name' => 'Laptop Hp Empresarial Core I5',
	        	'price' => 3500,
	        	'stock' => 123,
	        	'description' => 'Estándar : SDRAM DDR3 PC3-10600 (1333 MHz)* — Dos ranuras SODIMM compatibles con memoria de doble canal de 1024 MB, 2048 MB, 3072 MB o 4096 MB
Máxima : Ampliable a 8192 MB con módulos SODIMM de 4096 MB en las ranuras 1 y 2
Canal dual : El rendimiento maximizado de doble canal requiere módulos SODIMM del mismo tamaño y velocidad en ambas ranuras de memoria.',
	        	'category' => 'computadoras',
	        	'brand' => 'HP',
	        	'model' => 'HGTR239174',
	        	'organization_id' => 'pepe-sac'
	        ],
	        [
	        	'name' => 'Laptop Lenovo Core I7',
	        	'price' => 2890,
	        	'stock' => 123,
	        	'description' => 'PANTALLA    15.6 PULG LCD TFT LED / RESOLUCIÓN MAXIMA 1366X768
CPU    INTEL CORE i7 5500U 2.40 GHZ CACHE L3 4 MB
MEMORIA    8 GB
DISCO DURO    1 TB SATA 5400 RPM
OPTICO    DVD SUPERMULTI
LECTOR DE MEMORIAS    MMC/SD/SD-HC/SDXC
VIDEO    CHIPSET    INTEL HD GRAPHICS 5500
SALIDAS    HDMI / VGA
CONECTIVIDAD    WIRELESS / BLUETOOTH    4.0
SONIDO    PARLANTE STEREO / PUERTOS AUDIO/MIC
WEBCAM / TOUCHPAD
PUERTOS    USB 2.0 x 1 / USB 3.0 x 2 
BATERIA    4 CELDAS
SISTEMA OPERATIVO   FREE DOS',
	        	'brand' => 'Lenovo',
	        	'model' => 'DZ3459',
	        	'organization_id' => 'pepe-sac'
	        ],
	        [
	        	'name' => 'Canon Mg3510 Wifi Con Sistema Continuo Premium Black',
	        	'price' => 160,
	        	'stock' => 170,
	        	'description' => 'La impresora fotográfica multifuncional con conexión inalámbrica, PIXMA MG3510, brinda una excelente calidad de impresión y conveniencia en un diseño compacto. Su sistema de tintas híbrido combina la tinta de pigmento negro y de color para brindar un texto nítido y fotografías hermosas. Permite producir impresiones hermosas en el hogar con una resolución máxima de impresión en color de 4800 x 1200 dpi con la tecnología del cabezal de impresión FINE. La impresión automática a doble cara le permite imprimir automáticamente en ambos lados del papel y puede ayudar a reducir el uso y el costo del papel hasta un 50%. ',
	        	'category' => 'impresoras',
	        	'brand' => 'Canon',
	        	'model' => 'Mg3510',
	        	'organization_id' => 'pepe-sac'
	        ],
	        [
	        	'name' => 'Epson Workforce 7610 Multifuncion A3 Wifi Duplex Con Ciss',
	        	'price' => 990,
	        	'stock' => 45,
	        	'description' => 'Esta impresora con Wi-Fi y Ethernet 4-en-1 ofrece impresión rápida de calidad profesional hasta 18 ppm1 y una bandeja de papel de carga frontal para 250 hojas para una mayor productividad. La pantalla táctil de 10,9 cm y panel de control intuitivo permiten una navegación sencilla por los menús.',
	        	'category' => 'impresoras',
	        	'brand' => 'Epson',
	        	'model' => '7610',
	        	'organization_id' => 'pepe-sac'
	        ],
	        
        ];

        foreach ($items as $item) {
          Product::create($item);
        }
    }
}
