<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plate')->unique();
            $table->string('brand')->default('Brand');
            $table->string('model')->default('Model');
            $table->string('color')->default('Color');
            $table->integer('manufacture_year')->default(1950);
            $table->double('gas_consumption_rate')->default(0.00);
            $table->string('password', 60);
            $table->string('organization_id');
            $table->timestamps();

            $table->foreign('organization_id')
                ->references('id')
                ->on('organizations')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehicles');
    }
}
