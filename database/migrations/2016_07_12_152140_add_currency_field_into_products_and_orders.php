<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyFieldIntoProductsAndOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function ($table) {
            $table->string('currency_id')->default('PEN');
        });

        Schema::table('product_orders', function ($table) {
            $table->string('currency_id')->default('PEN');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function ($table) {
            $table->dropColumn('currency_id');
        });

        Schema::table('product_orders', function ($table) {
            $table->dropColumn('currency_id');
        });
    }
}
