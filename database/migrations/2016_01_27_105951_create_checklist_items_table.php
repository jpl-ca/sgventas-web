<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->boolean('checked')->default(false);
            $table->unsignedInteger('tasks_visit_point_id');
            $table->timestamps();

            $table->foreign('tasks_visit_point_id')
                ->references('id')
                ->on('tasks_visit_points')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checklist_items');
    }
}
